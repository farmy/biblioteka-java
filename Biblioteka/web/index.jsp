<%-- 
    Document   : index
    Created on : 2018-01-11, 11:16:22
    Author     : pawel
--%>


<%@page import="autoryzacja.Rejestracja"%>
<%@page import="komunikacja.Komunikator"%>
<%@page import="globalne.Pola"%>
<%@page import="autoryzacja.LogowanieServlet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pl">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Biblioteka</title>
        <!-- Bootstrap core CSS-->
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom fonts for this template-->
        <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- Custom styles for this template-->
        <link href="assets/css/sb-admin.css" rel="stylesheet">
        <link href="assets/css/main.css" rel="stylesheet">
    </head>

    <body class="bg-dark"> 
    

        <%
            // zaladuj warunkowo sesje do instancji aplikacji
            Pola.zaladujSesje(session);
            if (Pola.session.getAttribute(Pola.ACCOUNT_TYPE) != null) {
                switch ((Integer) Pola.session.getAttribute(Pola.ACCOUNT_TYPE)) {
                    case Pola.KLIENT: // klient
                       %>
        <%@include file="pages/user/user-index.jsp" %>
        <%
                break;
            case Pola.PRACOWNIK: // pracownik

        %>
        <%@include file="pages/mod/mod-index.jsp" %>
        <%
                break;
            case Pola.ADMINISTRATOR: // admin
        %>

        <%@include file="pages/admin/admin-index.jsp" %>
        <%
                    break;
            }
        } else {
        %>
        <%@include file="pages/ogolne/login.jsp" %>
        <%
            }
        %>

        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Page level plugin JavaScript-->
        <script src="assets/vendor/datatables/jquery.dataTables.js"></script>
        <script src="assets/vendor/datatables/dataTables.bootstrap4.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="assets/js/sb-admin.min.js"></script>
        <!-- Custom scripts for this page-->
        <script src="assets/js/sb-admin-datatables.min.js"></script>
    </body>

</html>
