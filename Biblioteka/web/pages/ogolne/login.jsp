<%-- 
    Document   : login
    Created on : 2018-01-11, 11:16:22
    Author     : pawel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pl">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Biblioteka</title>
  <!-- Bootstrap core CSS-->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="assets/css/sb-admin.css" rel="stylesheet">
  <link href="assets/css/main.css" rel="stylesheet">
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Zaloguj się</div>
      <div class="card-body">
        <form action="http://pawelglowacz.pl:8080/Biblioteka/LogowanieServlet" method="POST">
          <div class="form-group">
            <label for="login">Login</label>
            <input class="form-control" id="login" type="text" name="login" aria-describedby="emailHelp" placeholder="Login">
          </div>
          <div class="form-group">
            <label for="haslo">Hasło</label>
            <input class="form-control" id="haslo" name="haslo" type="password" placeholder="Hasło">
          </div>
           
          <div class="form-group" >
                          <label for="typ">Typ</label>
              <select name="typ" class="form-control">
                  <option value="0">Użytkownik</option>
                  <option value="1">Moderator</option>
                  <option value="2">Administrator</option>
              </select>
          </div>
            <input class="btn btn-primary btn-block" id="submit" type="submit" value="Zaloguj">
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="http://pawelglowacz.pl:8080/Biblioteka/pages/ogolne/register.jsp">Zarejestruj się</a>
          <a class="d-block small"href="http://pawelglowacz.pl:8080/Biblioteka/pages/ogolne/forgot-password.jsp">Zapomniałeś hasła?</a>
        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
