<%@include file="../ogolne/header-czysty-bezmenu.jsp" %>

<div class="container">
    <div class="card card-login mx-auto mt-5">
        <div class="card-header">Resetuj haslo</div>
        <div class="card-body">
            <div class="text-center mt-4 mb-5">
                <h4>Zapomniales hasla</h4>
                <p>Wpisz email konta, haslo zostanie zresetowane i przeslane na podany email.</p>
            </div>
            <form action="http://pawelglowacz.pl:8080/Biblioteka/OdzyskiwanieServlet" method="POST">
                <div class="form-group">
                    <input class="form-control" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" placeholder="Wpisz email" name="email">
                </div>
                <div class="form-group" >
                    <label for="typ">Typ</label>
                    <select name="typ" class="form-control">
                        <option value="0">U?ytkownik</option>
                        <option value="1">Moderator</option>
                        <option value="2">Administrator</option>
                    </select>
                </div>
                    <input class="btn btn-primary btn-block" id="submit" type="submit" value="Resetuj haslo">
            </form>
            <div class="text-center">
                <a class="d-block small mt-3" href="http://pawelglowacz.pl:8080/Biblioteka/pages/ogolne/register.jsp">Zarejestruj sie</a>
                <a class="d-block small" href="http://pawelglowacz.pl:8080/Biblioteka/index.jsp">Zaloguj sie</a>
            </div>
        </div>
    </div>
</div>
<%@include file="../ogolne/footer-czysty.jsp" %>
