<%@include file="header-czysty-bezmenu.jsp" %>


<body class="bg-dark">
    <div class="container">
        <div class="card card-register mx-auto mt-5">
            <div class="card-header">Zarejestruj sie</div>
            <div class="card-body">
                <form action="http://pawelglowacz.pl:8080/Biblioteka/RejestracjaServlet" method="POST">
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">Imie</label>
                                <input class="form-control" id="exampleInputName" type="text" aria-describedby="nameHelp" placeholder="Wpisz imie" name="imie">
                            </div>
                            <div class="col-md-6">
                                <label for="exampleInputLastName">Nazwisko</label>
                                <input class="form-control" id="exampleInputLastName" type="text" aria-describedby="nameHelp" placeholder="Wpisz nazwisko" name="nazwisko">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input class="form-control" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" placeholder="Wpisz email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="login">Login</label>
                        <input class="form-control" id="exampleInputLogin" type="text" aria-describedby="loginHelp" placeholder="Wpisz login" name="login">
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputPassword1">Haslo</label>
                                <input class="form-control" id="exampleInputPassword1" type="password" placeholder="Haslo" name="haslo">
                            </div>
                            <div class="col-md-6">
                                <label for="exampleConfirmPassword">Powtorz haslo</label>
                                <input class="form-control" id="exampleConfirmPassword" type="password" placeholder="Potwierdz haslo">
                            </div>
                        </div>
                    </div>
                    <label for="newsletter">Otrzymuj nowosci na email</label>
                    <input type="checkbox" name="newsletter" id="newsletter" class=""  checked="true"><br>
                    <input class="btn btn-primary btn-block" id="submit" type="submit" value="Zarejestruj sie">
                </form>
                <div class="text-center">
                    <a class="d-block small" href="http://pawelglowacz.pl:8080/Biblioteka/index.jsp">Zaloguj sie</a>
                    <a class="d-block small" href="http://pawelglowacz.pl:8080/Biblioteka/pages/ogolne/forgot-password.jsp">Zapomniales hasla?</a>
                </div>
            </div>
        </div>
    </div>
    <%@include file="footer-czysty.jsp" %>
