<%@page import="ksiazka.ZarzadzanieKsiazkami"%>
<div class="col-12">
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Ksiazki</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nazwa ksiazki</th>
                            <th>Autor</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Nazwa ksiazki</th>
                            <th>Autor</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <%
                            { // tworzymy prywatny scope zmiennych dla kazdego panelu.
                                final ZarzadzanieKsiazkami zarzadzanieKsiazkami = new ZarzadzanieKsiazkami();
                                final Object[][] ksiazkiSpis = zarzadzanieKsiazkami.pobierzKsiazki();
                                   for (int i = 0; i < ksiazkiSpis.length; i++) { %>
                        <tr>
                            <% for (int j = 1; j < ksiazkiSpis[i].length - 1; j++) {%>
                            <td><%=ksiazkiSpis[i][j]%></td>
                            <%    } %> 
                            <td> <a class="btn btn-success" href="http://pawelglowacz.pl:8080/Biblioteka/DodajKsiazkeDoPrzeczytaniaServlet?idKsiazka=<%=ksiazkiSpis[i][0] %>">Dodaj do chce przeczytac</a> </td>
                            <td> <a class="btn btn-success" href="http://pawelglowacz.pl:8080/Biblioteka/WypozyczKsiazkeServlet?nazwa=<%=ksiazkiSpis[i][1]%>&autor=<%=ksiazkiSpis[i][2]%>&sciezka=<%=ksiazkiSpis[i][3]%>">Czytaj</a> </td>
                        </tr>
                        <%  }
                            }%>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
