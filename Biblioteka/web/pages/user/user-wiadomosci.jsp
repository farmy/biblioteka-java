<%@page import="globalne.Pola"%>
<%@page import="komunikacja.KomunikacjaWewnetrzna"%>
<div class="col-12">
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Ksiazki
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nr</th>
                            <th>Tresc</th>
                            <th>Typ</th>
                            <th>Data utworzenia</th>
                            <th>Imie</th>
                            <th>Nazwisko</th>
                            <th>email</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Nr</th>
                            <th>Tresc</th>
                            <th>Typ</th>
                            <th>Data utworzenia</th>
                            <th>Imie</th>
                            <th>Nazwisko</th>
                            <th>email</th>
                        </tr>
                    </tfoot>
                    <tbody
                        <% { // tworzymy prywatny scope zmiennych dla kazdego panelu.
                                final KomunikacjaWewnetrzna listaPytanKlienta = new KomunikacjaWewnetrzna();
                                final Object[][] pytaniaKlienta = listaPytanKlienta.pobierzListePytanKlienta((Integer) Pola.session.getAttribute(Pola.ACCOUNT_ID));
                                for (int i = 0; i < pytaniaKlienta.length; i++) { %>
                        <tr>
                            <td><%=i+1%></td>
                            <% for (int j = 1; j < pytaniaKlienta[i].length; j++) {%>
                            <td><%=pytaniaKlienta[i][j]%></td>
                            <%    }%> 
                        </tr>
                        <%  }
                            }%>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
