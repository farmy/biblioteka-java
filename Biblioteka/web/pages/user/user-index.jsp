
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="index.html">Biblioteka</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                <a class="nav-link" href="http://pawelglowacz.pl:8080/Biblioteka/MenuServlet?menu=0">
                    <i class="fa fa-fw fa-dashboard"></i>
                    <span class="nav-link-text">Spis ksiazek</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
                <a class="nav-link" href="http://pawelglowacz.pl:8080/Biblioteka/MenuServlet?menu=1">
                    <i class="fa fa-fw fa-table"></i>
                    <span class="nav-link-text">Chce przeczytac</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
                <a class="nav-link" href="http://pawelglowacz.pl:8080/Biblioteka/MenuServlet?menu=2">
                    <i class="fa fa-fw fa-table"></i>
                    <span class="nav-link-text">Przeczytane</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
                <a class="nav-link" href="http://pawelglowacz.pl:8080/Biblioteka/MenuServlet?menu=3">
                    <i class="fa fa-fw fa-table"></i>
                    <span class="nav-link-text">Ustawienia</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
                <a class="nav-link" href="http://pawelglowacz.pl:8080/Biblioteka/MenuServlet?menu=4">
                    <i class="fa fa-fw fa-table"></i>
                    <span class="nav-link-text">Kontakt</span>
                </a>
            </li>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
                <a class="nav-link" href="http://pawelglowacz.pl:8080/Biblioteka/MenuServlet?menu=5">
                    <i class="fa fa-fw fa-table"></i>
                    <span class="nav-link-text">Wiadomosci</span>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav sidenav-toggler">
            <li class="nav-item">
                <a class="nav-link text-center" id="sidenavToggler">
                    <i class="fa fa-fw fa-angle-left"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="http://pawelglowacz.pl:8080/Biblioteka/WylogujServlet">
                    <i class="fa fa-fw fa-sign-out"></i>Wyloguj</a>
            </li>
        </ul>
    </div>
</nav>
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <% switch ((Integer) Pola.session.getAttribute(Pola.MENU)) {
                    case 0: // strona spis ksiazek
            %>
            <%@include file="user-spis.jsp" %>
            <%
                    break;                
                case 1: // chce przeczytac
            %>
            <%@include file="user-chce-przeczytac.jsp" %>
            <%
                    break;
                case 2: // przeczytane
            %>
            <%@include file="user-przeczytane.jsp" %>
            <%
                    break;
                case 3: // ustawienia
            %>
            <%@include file="user-ustawienia.jsp" %>

            <%
                    break;
                case 4: // kontakt
                    
            %>
            <%@include file="user-kontakt.jsp" %>
            <%
                    break;
                case 5: // kontakt
                    
            %>
            <%@include file="user-wiadomosci.jsp" %>
            <%
                    break;
                default: // zabezpieczenie
            %>
            <%@include file="user-kontakt.jsp" %>
            <%                   break;
                }%>
        </div>
    </div>
</div>

