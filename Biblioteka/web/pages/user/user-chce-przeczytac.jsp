<%@page import="globalne.Pola"%>
<%@page import="ksiazka.ZarzadzanieKsiazkamiDoPrzeczytania"%>
<div class="col-12">
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Ksiazki
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nazwa ksiazki</th>
                            <th>Autor</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Nazwa ksiazki</th>
                            <th>Autor</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody
                        <% { // tworzymy prywatny scope zmiennych dla kazdego panelu.
                                final ZarzadzanieKsiazkamiDoPrzeczytania zarzadzanieChcePrzeczytac = new ZarzadzanieKsiazkamiDoPrzeczytania();
                                final Object[][] chcePrzeczytac = zarzadzanieChcePrzeczytac.pobierzKsiazki((Integer) Pola.session.getAttribute(Pola.ACCOUNT_ID));
                                   for (int i = 0; i < chcePrzeczytac.length; i++) { %>
                        <tr>
                            <% for (int j = 1; j < chcePrzeczytac[i].length - 1; j++) {%>
                            <td><%=chcePrzeczytac[i][j]%></td>
                            <%    } %> 
                            <td> <a class="btn btn-success" href="http://pawelglowacz.pl:8080/Biblioteka/WypozyczKsiazkeServlet?nazwa=<%=chcePrzeczytac[i][1]%>&autor=<%=chcePrzeczytac[i][2]%>&sciezka=<%=chcePrzeczytac[i][3]%>">Czytaj</a> </td>
                            <td> <a class="btn btn-success" href="http://pawelglowacz.pl:8080/Biblioteka/UsunKsiazkeDoPrzeczytaniaServlet?id=<%=chcePrzeczytac[i][0]%>">Usun</a> </td>
                        </tr>
                        <%  }
                            }%>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
