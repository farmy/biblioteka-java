<%@page import="administracja.ZarzadzanieKontami"%>

<% { // private scope %>
<div class="col-lg-6 col-lg-offset-2">
    <form action="http://pawelglowacz.pl:8080/Biblioteka/DodajPytanieServlet" class="form-control">
        <label for="idPracownik">Pracownik</label>            
        <select class="form-control"  name="idPracownik">
            <% 
                final Object[][] pracownicy = new ZarzadzanieKontami().pobierzKontaPracownikow();
                // generuj opcje
                for (Object[] pracownik : pracownicy) {
            %>
            <option value="<%=pracownik[0]%>"><%=(pracownik[1] + " " + pracownik[2])%></option>
            <%
                }
            %>
        </select>

        <br>
        <label for="tresc">Tresc wiadomosci</label>
        <textarea name="tresc" id="tresc" cols="30" rows="10" class="form-control"></textarea>
        <br>
        <input type="submit" value="Wyslij" class="btn btn-block btn-danger">
    </form>
</div>
<% } // end of scope %>      