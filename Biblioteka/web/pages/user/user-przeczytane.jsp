<%@page import="ksiazka.ZarzadzanieKsiazkamiPrzeczytanymi"%>
<div class="col-12">
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Przeczytane
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Data</th>
                            <th>Nazwa ksiazki</th>
                            <th>Autor</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Data</th>
                            <th>Nazwa ksiazki</th>
                            <th>Autor</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <%
                            { // tworzymy prywatny scope zmiennych dla kazdego panelu.
                                final ZarzadzanieKsiazkamiPrzeczytanymi zarzadzanieKsiazkamiPrzeczytanymi = new ZarzadzanieKsiazkamiPrzeczytanymi();
                                final Object[][] ksiazkiPrzeczytane = zarzadzanieKsiazkamiPrzeczytanymi.pobierzKsiazki();
                                for (int i = 0; i < ksiazkiPrzeczytane.length; i++) { %>
                        <tr>
                            <% for (int j = 2; j < ksiazkiPrzeczytane[i].length; j++) {%>
                            <td><%=ksiazkiPrzeczytane[i][j]%></td>
                            <%    } %> 
                        </tr>
                        <%  }
                            }%>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
