<%@page import="globalne.Pola"%>
<%@page import="autoryzacja.LogowanieServlet"%>
<%@page import="konta.Profile"%>
<% // deklaracje
    final Profile profile = new Profile();
    final Object[] daneProfilu = profile.pobierzDaneKlienta((Integer) Pola.session.getAttribute(Pola.ACCOUNT_ID));
%>

<div class="col-6">
 <form action="http://pawelglowacz.pl:8080/Biblioteka/ProfilKlientaServlet" class="form-control">
        <label for="imie">Imie</label>
        <input type="text" name="imie" class="form-control" id="imie" value="<%=daneProfilu[1]%>">
        <br>
        <label for="nazwisko">Nazwisko</label>
        <input type="text" name="nazwisko" class="form-control" id="nazwisko" value="<%=daneProfilu[2]%>">
        <br>
        <label for="email">Email</label>
        <input type="text" name="email" class="form-control" id="email" value="<%=daneProfilu[3]%>">
        <br>
        <label for="login">Login</label>
        <input type="text" name="login" class="form-control" id="login" value="<%=daneProfilu[4]%>" readonly="true">
        <br>
        <label for="haslo">Zmien haslo</label>
        <input type="password" name="haslo" class="form-control" id="haslo" value="<%=daneProfilu[5]%>" >
        <br>
        <label for="newsletter">Otrzymuj nowosci na email</label>
        <input type="checkbox" name="newsletter" id="newsletter" class="" <%=((Boolean)daneProfilu[7] ? "checked" : "")%>>
        <br>
        <input type="submit" value="Zapisz" class="btn btn-block btn-danger">
    </form>
</div>    