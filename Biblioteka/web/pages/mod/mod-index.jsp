
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="index.jsp">Biblioteka</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                <a class="nav-link" href="http://pawelglowacz.pl:8080/Biblioteka/MenuServlet?menu=0">
                    <i class="fa fa-fw fa-dashboard"></i>
                    <span class="nav-link-text">Pytania</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
                <a class="nav-link" href="http://pawelglowacz.pl:8080/Biblioteka/MenuServlet?menu=1">
                    <i class="fa fa-fw fa-area-chart"></i>
                    <span class="nav-link-text">Ksiazki</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
                <a class="nav-link" href="http://pawelglowacz.pl:8080/Biblioteka/MenuServlet?menu=2">
                    <i class="fa fa-fw fa-table"></i>
                    <span class="nav-link-text">Ustawienia</span>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav sidenav-toggler">
            <li class="nav-item">
                <a class="nav-link text-center" id="sidenavToggler">
                    <i class="fa fa-fw fa-angle-left"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="http://pawelglowacz.pl:8080/Biblioteka/WylogujServlet">
                    <i class="fa fa-fw fa-sign-out"></i>Wyloguj</a>
            </li>
        </ul>
    </div>
</nav>

<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <% switch ((Integer) Pola.session.getAttribute(Pola.MENU)) {
                    case 0: // strona pytania
            %>
            <%@include file="mod-pytania.jsp" %>
            <%
                    break;
                case 1: // strona ksiazki
            %>
            <%@include file="mod-ksiazki.jsp" %>
            <%
                    break;
                case 2: // strona ustawienia
            %>
            <%@include file="mod-ustawienia.jsp" %>
            <%
                    break;
                default: // zabezpieczenie
            %>
            <%@include file="mod-ksiazki.jsp" %>
            <%                   break;
                            }%>
        </div>
    </div>
</div>

