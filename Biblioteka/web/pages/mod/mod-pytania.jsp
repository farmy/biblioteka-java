<%@page import="globalne.Pola"%>
<%@page import="komunikacja.KomunikacjaWewnetrzna"%>
<div class="col-12">
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Ksiazki
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nr</th>
                            <th>Tresc</th>
                            <th>Typ</th>
                            <th>Data utworzenia</th>
                            <th>Imie</th>
                            <th>Nazwisko</th>
                            <th>email</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Nr</th>
                            <th>Tresc</th>
                            <th>Typ</th>
                            <th>Data utworzenia</th>
                            <th>Imie</th>
                            <th>Nazwisko</th>
                            <th>email</th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody
                        <% { // tworzymy prywatny scope zmiennych dla kazdego panelu.
                                final KomunikacjaWewnetrzna komunikacjaWewnetrzna = new KomunikacjaWewnetrzna();
                                final Object[][] pytaniaPracownika = komunikacjaWewnetrzna.pobierzListePytanPracownika((Integer) Pola.session.getAttribute(Pola.ACCOUNT_ID));
                                for (int i = 0; i < pytaniaPracownika.length; i++) { %>
                        <tr>
                            <td><%=i+1%></td>
                            <% for (int j = 1; j < pytaniaPracownika[i].length - 1; j++) { // bez klient id%> 
                            <td><%=pytaniaPracownika[i][j]%></td>
                            <%    }%> 
                            <% // sprawdz czy jest to pytanie
                                if (pytaniaPracownika[i][2].equals("Pytanie")) {
                            %>
                                <td> <a href="http://pawelglowacz.pl:8080/Biblioteka/pages/mod/pytanie-strona.jsp?tresc=<%=pytaniaPracownika[i][1]%>&id=<%=pytaniaPracownika[i][7]%>" class="btn btn-success">Odpowiedz</a> </td>
                            <%
                                }
                            %>

                        </tr>
                        <%  }
                            }%>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
