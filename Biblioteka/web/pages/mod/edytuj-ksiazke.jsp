
<%@include file="../ogolne/header-czysty.jsp" %>

<% // deklaracje
    final Object[] daneKsiazki = new Object[]{
        request.getParameter("id"),
        request.getParameter("nazwa"),
        request.getParameter("autor"),
        request.getParameter("sciezka"),
    };
%>
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <form action="http://pawelglowacz.pl:8080/Biblioteka/EdytujKsiazkeServlet" class="form-control">
                    <label for="id">ID</label>
                    <input type="text" name="id" class="form-control" id="id" value="<%=daneKsiazki[0]%>" readonly="true">
                    <br>
                    <label for="nazwa">Nazwa</label>
                    <input type="text" name="nazwa" class="form-control" id="nazwa" value="<%=daneKsiazki[1]%>">
                    <br>
                    <label for="autor">Autor</label>
                    <input type="text" name="autor" class="form-control" id="autor" value="<%=daneKsiazki[2]%>">
                    <br>
                    <label for="sciezka">Sciezka</label>
                    <input type="text" name="sciezka" class="form-control" id="sciezka" value="<%=daneKsiazki[3]%>">
                    
                    <input type="submit" value="Zapisz" class="btn btn-block btn-danger">
                </form>
            </div>    
        </div>    
    </div>    
</div>    

<%@include file="../ogolne/footer-czysty.jsp" %>
