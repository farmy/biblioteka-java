<%@page import="ksiazka.ZarzadzanieKsiazkami"%>
<% { // tworzymy prywatny scope zmiennych dla kazdego panelu. %>

<div class="col-12">
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Lista ksaizek
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">                                
                    <thead>
                        <tr>
                            <th>Nazwa ksiazki</th>
                            <th>Autor</th>
                            <th>Sciezka</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Nazwa ksiazki</th>
                            <th>Autor</th>
                            <th>Sciezka</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody
                        <% 
                            final ZarzadzanieKsiazkami zarzadzanieKsiazkami = new ZarzadzanieKsiazkami();
                            final Object[][] ksiazki = zarzadzanieKsiazkami.pobierzKsiazki();
                            for (int i = 0; i < ksiazki.length; i++) { %>
                        <tr>
                            <% for (int j = 1; j < ksiazki[i].length; j++) {%>
                            <td><%=ksiazki[i][j]%></td>
                            <%    }%> 
                            <td> <a class="btn btn-success" href="http://pawelglowacz.pl:8080/Biblioteka/pages/mod/edytuj-ksiazke.jsp?id=<%=ksiazki[i][0]%>&nazwa=<%=ksiazki[i][1]%>&autor=<%=ksiazki[i][2]%>&sciezka=<%=ksiazki[i][3]%>">Edytuj</td>
                            <td> <a class="btn btn-success" href="http://pawelglowacz.pl:8080/Biblioteka/UsunKsiazkeServlet?id=<%=ksiazki[i][0]%>">Usun</a> </td>
                        </tr>
                        <%  }
                        %>
                    </tbody>
                </table>
            </div>
        </div>
        <hr>
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-plus"></i> Dodaj ksiazke
            </div>
            <div class="card-body">
                <form class="form-control" action="http://pawelglowacz.pl:8080/Biblioteka/DodajKsiazkeServlet"  method="post" enctype="multipart/form-data">
                    Nazwa:<input class="form-control" type="text" name="nazwa" /><br>
                    Autor:<input class="form-control" type="text" name="autor"/><br>
                    Plik:<input type="file" name="plik" class="form-control" /><br>
                    <input class="btn btn-block btn-danger" type="submit" value="Dodaj" /><br>    
                </form>
            </div>
        </div>

    </div>
</div>
<br>

<%
    } // koniec scope%>