<%@include file="../ogolne/header-czysty.jsp" %>
<% // dane
    final String[] dane = new String[]{
        request.getParameter("id"),
        request.getParameter("tresc"),};
%>

<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">


                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fa fa-plus"></i> Pytanie
                    </div>
                    <div class="card-body">
                        <%=dane[1]%>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">


                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fa fa-plus"></i> Dodaj odpowiedz
                    </div>
                    <div class="card-body">
                        <form method="post" class="form-control" action="http://pawelglowacz.pl:8080/Biblioteka/DodajOdpowiedzServlet">
                            Id klienta<input class="form-control" name="idKlient" type="text" readonly="true" value="<%=dane[0]%>"/>
                            <br>
                            Tresc <textarea class="form-control" name="tresc" /> </textarea>
                            <br>
                            <input class="btn btn-block btn-danger" type="submit" value="Dodaj" /><br>    
                        </form>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

<%@include file="../ogolne/footer-czysty.jsp" %>
