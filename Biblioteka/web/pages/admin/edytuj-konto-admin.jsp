<%@page import="konta.Profile"%>
<%@page import="autoryzacja.LogowanieServlet"%>
<%@include file="../ogolne/header-czysty.jsp" %>

<% // deklaracje
    final Profile profile = new Profile();
    int id = Integer.parseInt(request.getParameter("id"));

    final Object[] daneAdmina = profile.pobierzDaneAdministratora(id);
%>
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <form action="http://pawelglowacz.pl:8080/Biblioteka/EdytujKontoAdministratoraServlet" class="form-control">
                    <label for="id">ID</label>
                    <input type="text" name="id" class="form-control" id="id" value="<%=daneAdmina[0]%>" readonly="true">
                    <br>
                    <label for="imie">Imie</label>
                    <input type="text" name="imie" class="form-control" id="imie" value="<%=daneAdmina[1]%>">
                    <br>
                    <label for="nazwisko">Nazwisko</label>
                    <input type="text" name="nazwisko" class="form-control" id="nazwisko" value="<%=daneAdmina[2]%>">
                    <br>
                    <label for="email">Email</label>
                    <input type="text" name="email" class="form-control" id="email" value="<%=daneAdmina[3]%>">
                    <br>
                    <label for="login">Login</label>
                    <input type="text" name="login" class="form-control" id="login" value="<%=daneAdmina[4]%>" readonly="true">
                    <br>
                    <label for="haslo">Zmien haslo</label>
                    <input type="password" name="haslo" class="form-control" id="haslo" value="<%=daneAdmina[5]%>" >
                    <br>
                    <input type="hidden" name="id" class="form-control" id="haslo" value="<%=id%>" >

                    <input type="submit" value="Zapisz" class="btn btn-block btn-danger">
                </form>
            </div>    
        </div>    
    </div>    
</div>    

<%@include file="../ogolne/footer-czysty.jsp" %>
