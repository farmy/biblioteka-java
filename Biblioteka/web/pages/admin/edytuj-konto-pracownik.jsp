<%@page import="konta.Profile"%>
<%@page import="autoryzacja.LogowanieServlet"%>
<%@include file="../ogolne/header-czysty.jsp" %>

<% // deklaracje
    final Profile profile = new Profile();
    int id = Integer.parseInt(request.getParameter("id"));

    final Object[] danePracownika = profile.pobierzDanePracownika(id);
%>
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <form action="http://pawelglowacz.pl:8080/Biblioteka/EdytujKontoPracownikaServlet" class="form-control">
                    <label for="id">ID</label>
                    <input type="text" name="id" class="form-control" id="id" value="<%=danePracownika[0]%>" readonly="true">
                    <br>
                    <label for="imie">Imie</label>
                    <input type="text" name="imie" class="form-control" id="imie" value="<%=danePracownika[1]%>">
                    <br>
                    <label for="nazwisko">Nazwisko</label>
                    <input type="text" name="nazwisko" class="form-control" id="nazwisko" value="<%=danePracownika[2]%>">
                    <br>
                    <label for="email">Email</label>
                    <input type="text" name="email" class="form-control" id="email" value="<%=danePracownika[3]%>">
                    <br>
                    <label for="login">Login</label>
                    <input type="text" name="login" class="form-control" id="login" value="<%=danePracownika[4]%>" readonly="true">
                    <br>
                    <label for="haslo">Zmie? has?o</label>
                    <input type="password" name="haslo" class="form-control" id="haslo" value="<%=danePracownika[5]%>" >
                    <br>

                    <input type="submit" value="Zapisz" class="btn btn-block btn-danger">
                </form>
            </div>    
        </div>    
    </div>    
</div>    

<%@include file="../ogolne/footer-czysty.jsp" %>
