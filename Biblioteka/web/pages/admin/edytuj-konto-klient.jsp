<%@page import="autoryzacja.LogowanieServlet"%>
<%@page import="konta.Profile"%>
<%@include file="../ogolne/header-czysty.jsp" %>

<% // deklaracje
    final Profile profile = new Profile();
                int id = Integer.parseInt( request.getParameter("id") );
    final Object[] daneProfilu = profile.pobierzDaneKlienta(id);
%>
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <form action="http://pawelglowacz.pl:8080/Biblioteka/EdytujKontoKlientaServlet" class="form-control">
                    <label for="id">ID</label>
                    <input type="text" name="id" class="form-control" id="id" value="<%=daneProfilu[0]%>" readonly="true">
                    <br>
                    <label for="imie">Imie</label>
                    <input type="text" name="imie" class="form-control" id="imie" value="<%=daneProfilu[1]%>">
                    <br>
                    <label for="nazwisko">Nazwisko</label>
                    <input type="text" name="nazwisko" class="form-control" id="nazwisko" value="<%=daneProfilu[2]%>">
                    <br>
                    <label for="email">Email</label>
                    <input type="text" name="email" class="form-control" id="email" value="<%=daneProfilu[3]%>">
                    <br>
                    <label for="login">Login</label>
                    <input type="text" name="login" class="form-control" id="login" value="<%=daneProfilu[4]%>" readonly="true">
                    <br>
                    <label for="haslo">Zmien haslo</label>
                    <input type="password" name="haslo" class="form-control" id="haslo" value="<%=daneProfilu[5]%>" >
                    <br>
                    <label for="aktywne">Konto aktywne</label>
                    <input type="checkbox" name="aktywne" id="aktywne" class="" value="aktywne"  <%=((Boolean)daneProfilu[6] ? "checked" : "")%>>
                    <br>
                    <label for="newsletter">Otrzymuj nowosci na email</label>
                    <input type="checkbox" name="newsletter" id="newsletter" class="" value="newsletter" <%=((Boolean)daneProfilu[7] ? "checked" : "")%>>
                    <br>
                    <input type="submit" value="Zapisz" class="btn btn-block btn-danger">
                </form>
            </div></div>    </div>    </div>        

<%@include file="../ogolne/footer-czysty.jsp" %>
