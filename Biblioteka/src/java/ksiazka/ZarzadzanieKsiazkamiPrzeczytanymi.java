/*
 * Copyright 2017 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ksiazka;

import baza.Sterownik;
import baza.Struktura;
import java.sql.SQLException;

/**
 * Klasa odpowiada za funkcjonalnosc zarzadzania ksiazkami przeczytanymi.
 * @author marcin
 */
public class ZarzadzanieKsiazkamiPrzeczytanymi {   
    
     private final Sterownik sterownik = new Sterownik();
    
     // NARAZIE MAMY TYLKO DLA KLIENTA ODCZYT KSIAZEK, DODAWANIE ODBYWA SIE POPRZEZ WYPOZYCZANIE.
     
    /**
     * Pobierz liste ksiazek z bazy danych.
     * @return lista ksiazek.
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public Object[][] pobierzKsiazki() throws ClassNotFoundException, SQLException {
        // polacz z baza danych
        sterownik.connectToDatabase();

        Object[][] wynik = sterownik.queryDatabaseArray("SELECT * FROM " + Struktura.KSIAZKA_PRZECZYTANA, Struktura.KSIAZKA_PRZECZYTANA_COLLUMNS);

        // zakoncz polaczenie
        sterownik.closeConnection();

        return wynik;
    }
       /**
     * Pobierz liste ksiazek z bazy danych.
     * @return lista ksiazek.
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public Object[][] pobierzKsiazkiUzytkownika(int klientID) throws ClassNotFoundException, SQLException {
        // polacz z baza danych
        sterownik.connectToDatabase();

  Object[][] wynik = sterownik.queryDatabaseArray("select ID_KSIAZKA_PRZECZYTANA, KSIAZKA.TYTUL, KSIAZKA.AUTOR, DATA_PRZECZYTANIA \n"
                + "FROM KSIAZKA, KSIAZKA_PRZECZYTANA \n"
                + "WHERE KSIAZKA.ID_KSIAZKA = KSIAZKA_PRZECZYTANA.ID_KSIAZKA_PRZECZYTANA AND KSIAZKA_PRZECZYTANA.ID_KLIENT=" + klientID, new String[]{
                    "ID_KSIAZKA_PRZECZYTANA",
                    "TYTUL",
                    "AUTOR",
                    "DATA_PRZECZYTANIA"
                });

        // zakoncz polaczenie
        sterownik.closeConnection();

        return wynik;
    }
}
