/*
 * Copyright 2018 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ksiazka;

import globalne.BezpiecznyServlet;
import globalne.Drukarka;
import globalne.Pola;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author marcin
 */
@WebServlet("/upload") // temporary storage folder
@MultipartConfig
public class DodajKsiazkeServlet extends BezpiecznyServlet {

    @Override
    public boolean sprawdzDostep(HttpServletRequest request) { // warunek - uzytkownik zalogowany
        return Pola.session.getAttribute(Pola.ACCOUNT_TYPE) != null;
    }

    @Override
    public Object[] pobierzDane(HttpServletRequest request) throws IOException, ServletException {
        return new Object[]{
            request.getParameter("nazwa"),
            request.getParameter("autor"),
            request.getPart("plik")
        };
    }

    @Override
    public void wykonajPrace(HttpServletRequest request, Object[] dane, HttpServletResponse response) throws ClassNotFoundException, SQLException, IOException {
        try {
            final Part plik = (Part) dane[2];
            String sciezkaFolderu = this.getServletConfig().getServletContext().getRealPath("/") + "Ksiazki";
            dane[2] = sciezkaFolderu + File.separatorChar + plik.getSubmittedFileName(); // pobieramy sciezke
            //boolean sukces = new File(sciezkaFolderu).mkdir(); // stworz folder
            byte[] buffer = new byte[plik.getInputStream().available()]; // stworz bufor odczytu
            plik.getInputStream().read(buffer); // zapisz do bufora bajty pliku podanego przez uzytkownika
            FileOutputStream out = new FileOutputStream((String)dane[2]);
            out.write(buffer); // zapisz plik na serverze
            out.close();
            // zaladuj na server plik ksiazki.
            new ZarzadzanieKsiazkami().dodajKsiazke(dane, false);
        } catch (MessagingException ex) {
            przekierujPoPracy = false;
            response.getWriter().print(Drukarka.stworzStrone("Blad wiadomosci", "Nie udalo sie przeslac wiadomosci o dodaniu ksiazki!", "index.jsp"));
        }
    }

}
