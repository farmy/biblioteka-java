/*
 * Copyright 2018 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ksiazka;

import globalne.EdytorServlet;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author marcin
 */
public class EdytujKsiazkeStronaServlet extends EdytorServlet {

    @Override
    protected Object[] pobierzWartosci(HttpServletRequest request) {
        return new Object[]{
            request.getParameter("nazwa"),
            request.getParameter("autor"),
            request.getParameter("sciezka")
        };
    }

    @Override
    protected String stworzAkcje() {
        return "http://pawelglowacz.pl:8080/Biblioteka/EdytujKsiazkeServlet";
    }

    @Override
    protected String[] stworzPola() {
        return new String[] {
            "nazwa",
            "autor",
            "sciezka",
        };
    }


}
