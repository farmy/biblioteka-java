/*
 * Copyright 2017 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ksiazka;

import baza.Sterownik;
import baza.Struktura;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Klasa odpowiadajaca za funkcjonalnosc wypozyczania ksiazek.
 *
 * @author marcin
 */
public class Wypozyczanie {

    private final Sterownik sterownik = new Sterownik();

    /**
     * Wypozycz ksiazke. Dane powinny zostac pobrane z interfejsu wybranej
     * ksiazki.
     *
     * @param autor autor
     * @param tytul tytul
     * @param klientID id klienta.
     * @return true jesli operacja sie powiodla.
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public boolean wypozyczKsiazke(String autor, String tytul, int klientID) throws SQLException, ClassNotFoundException {
        // polacz z baza danych
        sterownik.connectToDatabase();

        // sprawdz czy ksiazka nie jest juz wypozyczona
        final Object[][] ksiazkaWypozyczona = sterownik.queryDatabaseArray("SELECT * FROM KSIAZKA_PRZECZYTANA WHERE AUTOR =" + sterownik.javaToSQL(autor) + " AND TYTUL = " + sterownik.javaToSQL(tytul), Struktura.KSIAZKA_PRZECZYTANA_COLLUMNS);
        
        if (ksiazkaWypozyczona.length == 0) { // wpisy nie istnieja, mozna dodac ksiazke
            // pobierz id nowego wiersza;
            final int noweID = sterownik.getNewID(Struktura.KSIAZKA_PRZECZYTANA);

            // przetworz wartosci java do sql
            Object[] wierszSQL = sterownik.javaToSQL(new Object[]{
                noweID,
                klientID,
                null,
                autor,
                tytul,});

            // get current date
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            String date = sdf.format(Calendar.getInstance().getTime());
            Object sqlDate = "STR_TO_DATE('" + date + "', '%Y/%m/%d %T')";

            wierszSQL[2] = sqlDate; // data nie moze byc przetwarzana

            // aktualizuj baze danych
            sterownik.insertIntoTable(Struktura.KSIAZKA_PRZECZYTANA, wierszSQL);
        }

        // zakoncz polaczenie
        sterownik.closeConnection();

        return true;
    }

    // UWAGA, robimy uproszczenie - ksiazka jest dostepna jezeli klient ma ja w historii czytanych, czyli ja wypozyczyl.
    /**
     * Sprawdz czy klient moze czytac tą książkę.
     *
     * @param klientID id klienta.
     * @param autor autor ksiazki.
     * @param tytul tytul ksiazki.
     * @return true jesli klient ma dostep do tej ksiazki.
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    public boolean sprawdzDostepDoKsiazki(int klientID, String autor, String tytul) throws ClassNotFoundException, SQLException {
        // polacz z baza danych
        sterownik.connectToDatabase();

        Object wynik[][] = sterownik.queryDatabaseArray("SELECT * FROM " + Struktura.KSIAZKA_PRZECZYTANA + " WHERE ID_" + Struktura.KLIENT + "=" + klientID
                + " AND AUTOR=" + sterownik.javaToSQL(autor) + " AND TYTUL=" + sterownik.javaToSQL(tytul), Struktura.KSIAZKA_PRZECZYTANA_COLLUMNS);

        // zakoncz polaczenie
        sterownik.closeConnection();

        return wynik.length == 1; // jest wpis, co oznacza ze klient ma dostep do ksiazki. 
    }

}
