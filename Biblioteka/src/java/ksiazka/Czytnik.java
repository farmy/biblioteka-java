/*
 * Copyright 2017 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ksiazka;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Klasa odpowiadajaca za odczyt zawartosci ksiazek. Uproszczenie - ksiazki w postaci txt.
 * @author marcin
 */
public class Czytnik {
    private Scanner scan;
    
    // todo: oczywiscie moznaby dodac epub, pdf itp ale to nie ma sensu i wymaga dodania bibliotek.
    
    // todo: PONIZSZA FUNKCJA ZJADA NEW LINE'Y ZOSTAWILEM TAK, BO NIE POWINNO TO MIEC ZNACZENIA - I TAK BEDZIEMY RECZNIE FORMATOWAC LINE-WRAP.
    /**
     * Odczytaj zawartosc ksiazki z pliku .txt
     * @param sciezkaDoKsiazki sciezka do ksiazki.
     * @return zawartosc ksiazki w postaci string builder'a
     */
    public StringBuilder odczytajKsiazke(String sciezkaDoKsiazki) {
        try {
            final StringBuilder builder = new StringBuilder();
            scan = new Scanner(new File(sciezkaDoKsiazki));
            while (scan.hasNextLine()) builder.append(scan.nextLine());
            scan.close();
            scan = null;        
            return builder;
        } catch (FileNotFoundException ex) {
            return new StringBuilder("Bledna sciezka do ksiazki!");
        }
    }
    
}
