/*
 * Copyright 2017 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ksiazka;

import baza.Sterownik;
import baza.Struktura;
import java.sql.SQLException;
import javax.mail.MessagingException;
import komunikacja.Komunikator;

/**
 * Klasa obsługująca funkcjonalność zarządzania i wyświetlania wpisów książek w
 * bazie danych.
 *
 * @author marcin
 */
public class ZarzadzanieKsiazkami {

    private final Sterownik sterownik = new Sterownik();
    private final Komunikator komunikator = new Komunikator();
    
    /**
     * Pobierz liste ksiazek z bazy danych.
     * @return lista ksiazek.
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public Object[][] pobierzKsiazki() throws ClassNotFoundException, SQLException {
        // polacz z baza danych
        sterownik.connectToDatabase();

        Object[][] wynik = sterownik.queryDatabaseArray("SELECT * FROM " + Struktura.KSIAZKA, Struktura.KSIAZKA_COLLUMNS);

        // zakoncz polaczenie
        sterownik.closeConnection();

        return wynik;
    }
    
    /**
     * Usun ksiazke z bazy danych.
     * @param ksiazkaID id ksiazki.
     * @return true jesli operacja sie powiodla.
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public boolean usunKsiazke(int ksiazkaID) throws ClassNotFoundException, SQLException {
        // polacz z baza danych
        sterownik.connectToDatabase();
        
        // usun klucze obce jesli istnieja
        sterownik.queryUpdate("DELETE FROM " + Struktura.KSIAZKA_CHCE_PRZECZYTAC + " WHERE ID_" + Struktura.KSIAZKA + "=" + ksiazkaID);
        
        // usun z tabeli ksiazka
        sterownik.queryUpdate("DELETE FROM " + Struktura.KSIAZKA + " WHERE ID_" + Struktura.KSIAZKA + "=" + ksiazkaID);

        // zakoncz polaczenie
        sterownik.closeConnection();

        return true;
    }
    
    /**
     * Dodaj ksiazke do bazy danych.
     * @param polaKsiazki pola ksiazki: tytyl, autor, sciezka
     * @param wyslijPowiadomienie true jesli zmiana ma zostac rozeslana do klientow z aktywnymi powiadomieniami.
     * @return true jesli operacja sie powiodla.
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public boolean dodajKsiazke(Object polaKsiazki[], boolean wyslijPowiadomienie) throws ClassNotFoundException, SQLException, MessagingException {
        // polacz z baza danych
        sterownik.connectToDatabase();

        // pobierz id nowego wiersza;
        final int noweID = sterownik.getNewID(Struktura.KSIAZKA);
        
        // przetworz wartosci java do sql
        Object daneTemp[] = new Object[polaKsiazki.length+1]; // potrzebujemy do danych dodac id
        daneTemp[0] = noweID;
        for (int i = 1; i < daneTemp.length; i++) { // skopiuj zawartosc danych
            daneTemp[i] = polaKsiazki[i-1];
        }
        daneTemp = sterownik.javaToSQL(daneTemp);
        
        // aktualizuj baze danych
        sterownik.insertIntoTable(Struktura.KSIAZKA, daneTemp);
        
        // zakoncz polaczenie
        sterownik.closeConnection();
        
        // wyslij powiadomienie jesli wszystko ok
        if (wyslijPowiadomienie) {
            komunikator.wyslijPowiadomienia(sterownik, polaKsiazki[2].toString(), polaKsiazki[1].toString());
        }
        
        return true;
    }
    
    /**
     * Edytuj ksiazke.
     * @param dane nowe pola ksiazki.
     * @param ksiazkaID id ksiazki.
     * @return true jesli operacja sie powiodla.
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public boolean edytujKsiazke(Object[] dane, int ksiazkaID) throws ClassNotFoundException, SQLException {
        // polacz z baza danych
        sterownik.connectToDatabase();

        // stworz zapytanie aktualizujace
        String sql = "UPDATE " + Struktura.KSIAZKA + " SET ";
        for (int i = 1; i < Struktura.KSIAZKA_COLLUMNS.length-1; i++) { // start od 1 aby pominac id
            sql += Struktura.KSIAZKA_COLLUMNS[i] + "=" + sterownik.javaToSQL(dane[i-1]) + ", "; // dane sa krotsze o 1
        }
        // ostatni element recznie, bo nie potrzebujemy przecinka
        sql += Struktura.KSIAZKA_COLLUMNS[Struktura.KSIAZKA_COLLUMNS.length-1] + "=" + sterownik.javaToSQL(dane[dane.length-1]) + " WHERE ID_" + Struktura.KSIAZKA + "=" + ksiazkaID;
        
        // aktualizuj baze danych
        sterownik.queryUpdate(sql);
        
        // zakoncz polaczenie
        sterownik.closeConnection();
        
        return true;
    }
    
}
