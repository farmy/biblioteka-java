/*
 * Copyright 2018 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ksiazka;

import globalne.BezpiecznyServlet;
import globalne.Pola;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author marcin
 */
public class EdytujKsiazkeServlet extends BezpiecznyServlet {

    @Override
    public boolean sprawdzDostep(HttpServletRequest request) {// warunek - uzytkownik zalogowany
        return Pola.session.getAttribute(Pola.ACCOUNT_TYPE) != null;
    }

    @Override
    public Object[] pobierzDane(HttpServletRequest request) { // zakladamy najprostsza opcje - nie sprawdzamy ktore pole sie zmienilo, aktualizujemy wszystkie.        
        return new Object[]{
            request.getParameter("nazwa"),
            request.getParameter("autor"),
            request.getParameter("sciezka"),
            request.getParameter("id")
        };
    }

    @Override
    public void wykonajPrace(HttpServletRequest request, Object[] dane, HttpServletResponse response) throws ClassNotFoundException, SQLException, IOException {
        new ZarzadzanieKsiazkami().edytujKsiazke(Arrays.copyOf(dane, 3), new Integer(dane[3].toString())); // przekazujemy tylko pierwsze 3 pola - bez id, id jest przekazywane oddzielnie
    }


}
