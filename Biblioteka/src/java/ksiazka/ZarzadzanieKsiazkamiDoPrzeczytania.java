/*
 * Copyright 2017 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ksiazka;

import baza.Sterownik;
import baza.Struktura;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Klasa odpowiada za funkcjonalnosc zarzadzania ksiazkami w tabeli chce przeczytac.
 * @author marcin
 */
public class ZarzadzanieKsiazkamiDoPrzeczytania {

    private final Sterownik sterownik = new Sterownik();

    /**
     * Pobierz liste ksiazek z bazy danych dla danego klienta. UWAGA PIERWSZE
     * POLE NIE ODPOWIADA RZECZYWISTEJ NUMERACJI DLATEGO WARTO WPROWADZIC WLASNA
     * NUMERACJE A POLE ZACHOWAC DO OPERACJI WYKONYWANYCH W TABELACH.
     *
     * @param klientID id klienta.
     * @return lista ksiazek.
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public Object[][] pobierzKsiazki(int klientID) throws ClassNotFoundException, SQLException {
        // polacz z baza danych
        sterownik.connectToDatabase();

        Object[][] wynik = sterownik.queryDatabaseArray("select ID_KSIAZKA_CHCE_PRZECZYTAC, KSIAZKA.TYTUL, KSIAZKA.AUTOR, DATA_DODANIA \n"
                + "FROM KSIAZKA, KSIAZKA_CHCE_PRZECZYTAC \n"
                + "WHERE KSIAZKA.ID_KSIAZKA = KSIAZKA_CHCE_PRZECZYTAC.ID_KSIAZKA AND KSIAZKA_CHCE_PRZECZYTAC.ID_KLIENT=" + klientID, new String[]{
                    "ID_KSIAZKA_CHCE_PRZECZYTAC",
                    "TYTUL",
                    "AUTOR",
                    "DATA_DODANIA"
                });

        // zakoncz polaczenie
        sterownik.closeConnection();

        return wynik;
    }

    /**
     * Usun ksiazke z bazy danych.
     *
     * @param ksiazkaID id ksiazki.
     * @return true jesli operacja sie powiodla.
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public boolean usunKsiazke(int ksiazkaID) throws ClassNotFoundException, SQLException {
        // polacz z baza danych
        sterownik.connectToDatabase();

        sterownik.queryUpdate("DELETE FROM " + Struktura.KSIAZKA_CHCE_PRZECZYTAC + " WHERE ID_" + Struktura.KSIAZKA_CHCE_PRZECZYTAC + "=" + ksiazkaID);

        // zakoncz polaczenie
        sterownik.closeConnection();

        return true;
    }

    /**
     * Dodaj ksiazke do bazy danych.
     *
     * @param ksiazkaID id ksiazki.
     * @param klientID id klienta.
     * @return true jesli operacja sie powiodla.
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public boolean dodajKsiazke(int ksiazkaID, int klientID) throws ClassNotFoundException, SQLException {
        // polacz z baza danych
        sterownik.connectToDatabase();

        // pobierz id nowego wiersza;
        final int noweID = sterownik.getNewID(Struktura.KSIAZKA_CHCE_PRZECZYTAC);

        // przetworz wartosci java do sql
        Object[] wierszSQL = sterownik.javaToSQL(new Object[]{
            noweID,
            ksiazkaID,
            klientID,
            null,
        });

         // get current date
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String date = sdf.format(Calendar.getInstance().getTime());
        Object sqlDate = "STR_TO_DATE('" + date + "', '%Y/%m/%d %T')";
        
        wierszSQL[3] = sqlDate; // data nie moze byc przetwarzana
        
        // aktualizuj baze danych
        sterownik.insertIntoTable(Struktura.KSIAZKA_CHCE_PRZECZYTAC, wierszSQL);

        // zakoncz polaczenie
        sterownik.closeConnection();

        return true;
    }

}
