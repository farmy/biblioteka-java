/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baza;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.TreeMap;
import javax.validation.constraints.NotNull;

/**
 * Sterownik bazy danych, umozliwiajacy operacje na bazie w kodzie. Interfejs
 * moze uzywac narzedzi wbudowanych w web-app.
 *
 * @author marcin
 */
public class Sterownik {

    @NotNull
    private Connection connection;

// db admin, aoldeskop789
    /**
     * Metoda pozwalajaca połączyć się z bazą danych. Nie pobiera żadnych
     * parametrów ponieważ w obrębie naszej aplikacji korzystamy tylko z 1
     * zestawu parametrów połączenia.
     *
     * @throws ClassNotFoundException Jeżeli aplikacja nie ma odpowiedniej
     * biblioteki.
     * @throws SQLException Błąd parametrów połączenia.
     */
    public void connectToDatabase()
            throws ClassNotFoundException, SQLException {
        String userName = "admin";
        String password = "aoldeskop789";

        Class.forName("com.mysql.jdbc.Driver");

        // URL Connection for MySQL:
        // Example: 
        // jdbc:mysql://localhost:3306/simplehr
        String connectionURL = "jdbc:mysql://pawelglowacz.pl:3306/biblioteka";

        connection = DriverManager.getConnection(connectionURL, userName,
                password);
    }

    /**
     * Query database, return sorted map as result.
     *
     * @param query sql query
     * @param collumns columns which are part of sql result.
     * @return sql sorted map, under every column name there is mapped array
     * list containing all records.
     * @throws java.sql.SQLException exception if sql operation failed.
     */
    public TreeMap<String, ArrayList<Object>> queryDatabase(String query, String[] collumns) throws SQLException {
        final Statement stmt = connection.createStatement(); // create sql statement - object which handles sql query execution
        final ResultSet rs = stmt.executeQuery(query); // query database and store result as ResultSet
        // create and initialize map
        final TreeMap<String, ArrayList<Object>> result = new TreeMap<>();
        for (String collumn : collumns) {
            result.put(collumn, new ArrayList<>());
        }
        // for every record find corresponding collumn and add it to list.
        while (rs.next()) {
            for (String collumn : collumns) {
                result.get(collumn).add(rs.getObject(collumn));
            }
        }
        return result;
    }

    /**
     * Query database, return double dimension array as result. Look to query
     * databse for details
     *
     * @param query query to database.
     * @param collumns collumns which will be in result of query.
     * @return result of query in two-dimensional array format.
     * @throws java.sql.SQLException exception if sql operation failed.
     */
    public Object[][] queryDatabaseArray(String query, String[] collumns) throws SQLException {
        final Statement stmt = connection.createStatement();
        final ResultSet rs = stmt.executeQuery(query);
        // create temporary list
        final ArrayList<Object[]> resultList = new ArrayList<>();
        int counter = 0;
        Object[] tableRow;
        while (rs.next()) {
            // fill temporary list with results of query
            tableRow = new Object[collumns.length];
            for (String kolumna : collumns) { // for every element in a row
                // initialize array
                tableRow[counter] = rs.getObject(kolumna);
                counter++;
            }
            resultList.add(tableRow);
            counter = 0;
        }
        // transform temporary list to two dimensional array
        Object[][] result = new Object[resultList.size()][];
        for (Object[] o : resultList) {
            result[counter] = o;
            counter++;
        }
        return result;
    }

    /**
     * Query database with no result - for example update, set etc.
     *
     * @param query query to database.
     * @throws java.sql.SQLException exception if sql operation failed.
     */
    public void queryUpdate(String query) throws SQLException {
        final Statement stmt = connection.createStatement();
        stmt.executeUpdate(query);
    }

    /**
     * This method calculate new id for inserting fields in sql table - it bases
     * on assumption that field with id has name ID_tablename
     *
     * @param tableName table name.
     * @return new id.
     * @throws java.sql.SQLException exception if sql operation failed.
     */
    public Integer getNewID(String tableName) throws SQLException {
        // get last id from table
        String fieldName = "ID_" + tableName;
        Object[][] queryResult = queryDatabaseArray("SELECT " + fieldName
                + " FROM " + tableName
                + " ORDER BY " + fieldName + " DESC",
                new String[]{fieldName});
        Object lastID = (queryResult.length > 0) ? queryResult[0][0] : 0;
        // return last id +1
        return new Integer(lastID.toString()) + 1;
    }

    /**
     * This method translate fields values from java to sql, for example string
     * from text to 'text' format.
     *
     * @param row row of fileds to translate.
     * @return translated row.
     */
    public Object[] javaToSQL(Object[] row) {
        for (int i = 0; i < row.length; i++) {
            if (row[i] == null) {
                row[i] = "NULL";
            } else if (row[i] instanceof String) {
                row[i] = "'" + row[i] + "'";
            }
        }
        return row;
    }

    /**
     * Translate java string to sql.
     *
     * @param object java object.
     * @return sql object.
     */
    public Object javaToSQL(Object object) {
        if (object == null) {
            return  "NULL";
        } else if (object instanceof String) {
            return  "'" + object + "'";
        } else 
            return object;
    }

    /**
     * Insert into table row of values, remember that insert works only for all
     * fields! REMEMBER TO PARSE JAVA VALUES FIRST.
     *
     * @param tableName tableName.
     * @param row row of fields.
     * @throws SQLException if error occured while connecting to database.
     */
    public void insertIntoTable(Object tableName, Object[] row) throws SQLException {
        String query = "INSERT INTO " + tableName + " VALUES(";
        for (int i = 0; i < row.length - 1; i++) {
            query += row[i] + ", ";
        }
        // last item manually
        query += row[row.length - 1] + ")";
        queryUpdate(query);
    }

    /**
     * Close database connection.
     *
     * @throws SQLException exception if failed to close connection.
     */
    public void closeConnection() throws SQLException {
        connection.close();
        connection = null;
    }

}
