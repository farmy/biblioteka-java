/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  marcin
 * Created: 2017-12-12
 */

insert into KLIENT(ID_KLIENT, IMIE, NAZWISKO, EMAIL, LOGIN, HASLO, AKTYWNE, POWIADOMIENIE_AKTYWNE)
VALUES(1, 'MAREK', 'NOWAK', 'M@NOWAK.COM', 'KLIENT', 'KLIENT123', 1, 1);

insert into PRACOWNIK(ID_PRACOWNIK, IMIE, NAZWISKO, EMAIL, LOGIN, HASLO)
VALUES(1, 'Pracus', 'Test', 'prac@test.COM', 'PRACOWNIK', 'PRACOWNIK123');

insert into ADMINISTRATOR(ID_ADMINISTRATOR, IMIE, NAZWISKO, EMAIL, LOGIN, HASLO)
VALUES(1, 'Admin', 'Hart', 'admin@test.COM', 'ADMINISTRATOR', 'ADMINISTRATOR123');