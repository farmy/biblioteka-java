/*
 * Copyright 2017 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package baza;

/**
 * Klasa zawierajaca strukture bazy danych.
 * @author marcin
 */
public class Struktura {
    /**
     * Nazwy tabel.
     */
    public static final String KLIENT = "KLIENT", 
            PRACOWNIK = "PRACOWNIK", 
            ADMINISTRATOR = "ADMINISTRATOR", 
            WIADOMOSC = "WIADOMOSC", 
            KSIAZKA = "KSIAZKA",
            KSIAZKA_CHCE_PRZECZYTAC = "KSIAZKA_CHCE_PRZECZYTAC",
            KSIAZKA_PRZECZYTANA = "KSIAZKA_PRZECZYTANA";
    
    /**
    * Pola tabel.
    */
    public static final String[] KLIENT_COLLUMNS = new String[]{
        "ID_KLIENT",
        "IMIE",
        "NAZWISKO",
        "EMAIL",
        "LOGIN",
        "HASLO",
        "AKTYWNE",
        "POWIADOMIENIE_AKTYWNE",        
    }, KLIENT_COLLUMNS_PROFILE = new String[]{
        "ID_KLIENT",
        "IMIE",
        "NAZWISKO",
        "EMAIL",
        "LOGIN",
        "HASLO",
        "POWIADOMIENIE_AKTYWNE",        
    }, PRACOWNIK_COLLUMNS = new String[]{
        "ID_PRACOWNIK",
        "IMIE",
        "NAZWISKO",
        "EMAIL",
        "LOGIN",
        "HASLO"       
    }, ADMINISTRATOR_COLLUMNS = new String[]{
        "ID_ADMINISTRATOR",
        "IMIE",
        "NAZWISKO",
        "EMAIL",
        "LOGIN",
        "HASLO"      
    }, WIADOMOSC_COLLUMNS = new String[]{
        "ID_WIADOMOSC",
        "ID_KLIENT",
        "ID_PRACOWNIK",
        "TRESC",
        "TYP",
        "DATA_UTWORZENIA",
    }, KSIAZKA_COLLUMNS = new String[]{
        "ID_KSIAZKA",
        "TYTUL",
        "AUTOR",
        "SCIEZKA_DOSTEPU"
    }, KSIAZKA_CHCE_PRZECZYTAC_COLLUMNS = new String[]{
        "ID_KSIAZKA_CHCE_PRZECZYTAC",
        "ID_KSIAZKA",
        "ID_KLIENT",
        "DATA_DODANIA"
    }, KSIAZKA_PRZECZYTANA_COLLUMNS = new String[]{
        "ID_KSIAZKA_PRZECZYTANA",
        "ID_KLIENT",
        "DATA_PRZECZYTANIA",
        "AUTOR",
        "TYTUL"
    };
}
