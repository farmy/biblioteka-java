/*
 * Copyright 2018 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package globalne;

/**
 * Klasa generujaca proste strony komunikujace zdarzenia w serwisie.
 * @author marcin
 */
public class Drukarka {
    
    /**
     * Tworzy prosta strone komunikatu.
     * @param tytul tytul komunikatu.
     * @param tresc tresc komunikatu.
     * @param linkPowrotny link powrotu z komunikatu.
     * @return strone komunikatu.
     */
    public static String stworzStrone(String tytul, String tresc, String linkPowrotny) {
        return "<html>\n"
                + "    <head>\n"
                + "        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n"
                + "        <title>" + tytul + "</title>\n"
                + "    </head>\n"
                + "    <body>\n"
                + "        <div style=\"width: 1200px; margin: auto\">" + tresc
                + "<br><a style=\"font-size : 24px\" href=\"" + linkPowrotny + "\">powrot</a>"
                + "</div>"
                + "    </body>\n"
                + "</html>";
    }
}
