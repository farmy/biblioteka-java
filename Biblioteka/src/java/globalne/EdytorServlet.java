/*
 * Copyright 2018 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package globalne;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author marcin
 */
public abstract class EdytorServlet extends BezpiecznyServlet {

    protected abstract Object[] pobierzWartosci(HttpServletRequest request);

    protected abstract String stworzAkcje();

    protected abstract String[] stworzPola();

    @Override
    public boolean sprawdzDostep(HttpServletRequest request) {
        return Pola.session.getAttribute(Pola.ACCOUNT_TYPE) != null; // sprawdz czy uzytkownik zalogowany
    }

    @Override
    public Object[] pobierzDane(HttpServletRequest request) {
        return new Object[]{
            request.getParameter("id"),
            request.getParameter("ilosc"),
            pobierzWartosci(request)
        };
    }

    @Override
    public void wykonajPrace(HttpServletRequest request, Object[] dane, HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");
        super.przekierujPoPracy = false; // nie chcemy auto przekierowania.
        // generuj tresc strony edycyjnej 
        final String tytul = "Edycja",
                linkPowrotny = "index.jsp";
        // formularz edycyjny tworzony dynamicznie
        String tresc = "<form action=\"" + stworzAkcje() + "\" class=\"form-control\" method=\"POST\">\n" 
                + "<label for=\"id\">id</label>\n" +
"        <input readonly=\"true\" type=\"text\" name=\"id\" class=\"form-control\" id=\"id\" value=\"" + dane[0] + "\">\n" +
"        <br>";

        final int ilosc = new Integer(dane[1].toString());
        final String[] pola = stworzPola();
        for (int i = 0; i < ilosc; i++) {
            tresc += "        <label for=\"" + pola[i] + "\">" + pola[i] + "</label>\n"
                    + "        <input type=\"text\" name=\"" + pola[i] + "\" class=\"form-control\" id=\"" + pola[i] + "\" value=\"" + ((Object[]) dane[2])[i] + "\">\n"
                    + "        <br>\n";
        }
        tresc += "        <input type=\"submit\" value=\"Zapisz\" class=\"btn btn-block btn-danger\">"
                + "</form>";
        // stworz strone edycyjna
        response.getWriter().println(Drukarka.stworzStrone(tytul, tresc, linkPowrotny));
    }

}
