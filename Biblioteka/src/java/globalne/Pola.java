/*
 * Copyright 2018 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package globalne;

import javax.servlet.http.HttpSession;

/**
 *
 * @author marcin
 */
public class Pola {
    public static HttpSession session = null;
    
    /**
     * Warunkowo zaladuj instancje sesji do instancji aplikacji. Funkcja ta powinna zostac wywolana w glownym indexie. Sesja jest ladowana, gdy nie istnieje instancja lub instancja jest zniszczona.
     * @param session sesja generowana przez glassfish.
     */
    public static void zaladujSesje(HttpSession session) {
        if (Pola.session == null) { // jezeli instancja sesji nie istnieje
            Pola.session = session;
        } else { // jezeli sesja istnieje sprawdz czy jest wazna
            try {
                Pola.session.getAttribute(ACCOUNT_TYPE);
            } catch (IllegalStateException ex) { // sesja jest nie wazna
                Pola.session = session;
            }
        }
    }
    
    public static final int KLIENT = 0, PRACOWNIK = 1, ADMINISTRATOR = 2;
    public static final String ACCOUNT_ID = "idKonta", ACCOUNT_TYPE = "typKonta", MENU = "menu";
}
