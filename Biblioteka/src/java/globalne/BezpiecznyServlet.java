/*
 * Copyright 2018 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package globalne;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Archetyp servletu - zapewnia bezpieczne wykonanie operacji przez aplikacje.
 *
 * @author marcin
 */
public abstract class BezpiecznyServlet extends HttpServlet {

    protected boolean przekierujPoPracy = true;

    protected abstract boolean sprawdzDostep(HttpServletRequest request);

    protected abstract Object[] pobierzDane(HttpServletRequest request) throws IOException, ServletException;

    protected abstract void wykonajPrace(HttpServletRequest request, Object[] dane, HttpServletResponse response) throws ClassNotFoundException, SQLException, IOException;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // zabezpieczenie przed bezposrednim wejsciem do servletu
        if (sprawdzDostep(request)) {
            // pobierz dane przekazane przez formularz
            final Object[] dane = pobierzDane(request);
            if (sprawdzPoprawnoscDanych(dane)) { // sprawdz poprawnosc danych
                try {
                    przekierujPoPracy = true; // ustal przekierowywanie domyslnie true
                    wykonajPrace(request, dane, response);
                    // przekieruj do glownej strony - odpowiedni widok wyswietli sie na podstawie typuKotna
                    if (przekierujPoPracy) {
                        response.sendRedirect("index.jsp");
                    }
                } catch (ClassNotFoundException ex) {
                    response.getWriter().print(Drukarka.stworzStrone("Blad aplikacji", "Blad modulu aplikacji - nie udalo sie zalaczyc sterownika bazy danych, skontaktuj sie z obsluga serwisu.!", "index.jsp"));
                } catch (SQLException ex) {
                    response.getWriter().print(Drukarka.stworzStrone("Blad bazy danych", "Nie udalo sie wykonac operacji w bazie danych, skontaktuj sie z obsluga serwisu! Przyczyna: " + ex.getMessage(), "index.jsp"));
                }
            } else { // dane sa wstepnie niepoprawne - puste lub null
                response.getWriter().print(Drukarka.stworzStrone("Blad danych", "Pola musza byc wypelnione!", "index.jsp"));
            }
        }
    }

    private boolean sprawdzPoprawnoscDanych(Object[] dane) {
        for (Object dana : dane) {
            if (dana == null) {
                return false;
            }
            if (dana instanceof String && ((String) dana).isEmpty()) {
                return false;
            }
        }
        return true;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
