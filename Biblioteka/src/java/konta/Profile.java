/*
 * Copyright 2017 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package konta;

import baza.Sterownik;
import baza.Struktura;
import java.sql.SQLException;

/**
 * Klasa odpowiadajaca wyswietlanie/edycje profili kont uzytkownikow.
 *
 * @author marcin
 */
public class Profile {

    private final Sterownik sterownik = new Sterownik();

    private Object[] pobierzDaneProfilu(String nazwa, String[] kolumny, int wierszID) throws ClassNotFoundException, SQLException {
        // polacz z baza danych
        sterownik.connectToDatabase();

        Object[] wynik = sterownik.queryDatabaseArray("SELECT * FROM " + nazwa + " WHERE ID_" + nazwa + "=" + wierszID, kolumny)[0]; // interesuje nas tylko 1 wpis - primary key jest unique

        // zakoncz polaczenie
        sterownik.closeConnection();

        return wynik;
    }
    
    /**
     * Pobierz dane profilu konta klienta.
     * @param klientID id konta.
     * @return wszystkie dane profilu klienta.
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public Object[] pobierzDaneKlienta(int klientID) throws ClassNotFoundException, SQLException {
        return pobierzDaneProfilu(Struktura.KLIENT, Struktura.KLIENT_COLLUMNS, klientID);
    }

    /**
     * Pobierz dane profilu konta pracownika.
     * @param pracownikID id konta.
     * @return wszystkie dane profilu pracownika.
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public Object[] pobierzDanePracownika(int pracownikID) throws ClassNotFoundException, SQLException {
        return pobierzDaneProfilu(Struktura.PRACOWNIK, Struktura.PRACOWNIK_COLLUMNS, pracownikID);
    }

    /**
     * Pobierz dane profilu konta administratora.
     * @param administratorID id konta.
     * @return wszystkie dane profilu administratora.
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public Object[] pobierzDaneAdministratora(int administratorID) throws ClassNotFoundException, SQLException {
        return pobierzDaneProfilu(Struktura.ADMINISTRATOR, Struktura.ADMINISTRATOR_COLLUMNS, administratorID);
    }
    
    // PODEJSCIE DOBRE JEZELI ZMIANY SA STOSOWANE DLA CALEGO ZESTAWU DANYCH, NP PO NACISNIECIU PRZYCISKU ZAPISZ ZMIANY
    // todo: mozliwa optymalizacja zapytan ale to raczej dla wielkich serwisow - pomijanie wartosci null w danych aby odciazyc baze.
    private boolean aktualizujDaneProfilu(Object[] dane, String[] kolumny, String nazwa, int wierszID) throws ClassNotFoundException, SQLException {
        // polacz z baza danych
        sterownik.connectToDatabase();

        // stworz zapytanie aktualizujace
        String sql = "UPDATE " + nazwa + " SET ";
        for (int i = 1; i < kolumny.length-1; i++) { // start od 1 aby pominac id
            sql += kolumny[i] + "=" + sterownik.javaToSQL(dane[i-1]) + ", "; // dane sa krotsze o 1
        }
        // ostatni element recznie, bo nie potrzebujemy przecinka
        sql += kolumny[kolumny.length-1] + "=" + sterownik.javaToSQL(dane[dane.length-1]) + " WHERE ID_" + nazwa + "=" + wierszID;
        
        // aktualizuj baze danych
        sterownik.queryUpdate(sql);
        
        // zakoncz polaczenie
        sterownik.closeConnection();
        
        return true;
    }
    
    
    /**
     * Aktualizuj dane profilu klienta.
     * @param dane nowe dane, bez pola klucza glownego.
     * @param klientID id klienta.
     * @param kolumny kolumny ktore maja ulec zmianie, pamietac aby byly tozsame z danymi.
     * @return true jesli operacja sie powiodla.
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public boolean aktualizujDaneKlienta(Object[] dane, int klientID, String[] kolumny) throws ClassNotFoundException, SQLException {
        return aktualizujDaneProfilu(dane, kolumny, Struktura.KLIENT, klientID);
    }
    
    /**
     * Aktualizuj dane profilu pracownika.
     * @param dane nowe dane, bez pola klucza glownego.
     * @param pracownikID id pracownika.
     * @return true jesli operacja sie powiodla.
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public boolean aktualizujDanePracownika(Object[] dane, int pracownikID) throws ClassNotFoundException, SQLException {
        return aktualizujDaneProfilu(dane, Struktura.PRACOWNIK_COLLUMNS, Struktura.PRACOWNIK, pracownikID);
    }
    
    /**
     * Aktualizuj dane profilu administratora.
     * @param dane nowe dane, bez pola klucza glownego.
     * @param administratorID id administratora.
     * @return true jesli operacja sie powiodla.
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public boolean aktualizujDaneAdministratora(Object[] dane, int administratorID) throws ClassNotFoundException, SQLException {
        return aktualizujDaneProfilu(dane, Struktura.ADMINISTRATOR_COLLUMNS, Struktura.ADMINISTRATOR, administratorID);
    }

}
