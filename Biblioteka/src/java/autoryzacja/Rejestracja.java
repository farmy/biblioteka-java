/*
 * Copyright 2017 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package autoryzacja;

import baza.Sterownik;
import baza.Struktura;
import java.sql.SQLException;
import javax.mail.MessagingException;
import komunikacja.Komunikator;

/**
 * Klasa obsługująca rejestracje.
 *
 * @author marcin
 */
public class Rejestracja {

    private final Sterownik sterownik = new Sterownik();

    /**
     * Dodaj konto klienta do bazy danych. Automatycznie wysyla wiadomosc z
     * linkiem aktywujacym.
     *
     * @param imie imie klienta.
     * @param nazwisko nazwisko klienta.
     * @param email email klienta.
     * @param login login klienta.
     * @param haslo haslo klienta.
     * @param powiadomienia true jesli ma wlaczone powiadomienia.
     * @return true jezeli operacja sie powiodla.
     * @throws java.lang.ClassNotFoundException brak biblioteki w projekcie.
     * @throws java.sql.SQLException bledne dane w sterowniku.
     */
    public boolean dodajKontoKlienta(String imie, String nazwisko, String email, String login, String haslo, boolean powiadomienia) throws ClassNotFoundException, SQLException, MessagingException {
        // polacz z baza danych
        sterownik.connectToDatabase();

        // sprawdz unikalność loginu.
        if (!sprawdzCzyLoginUnique(login)) {
            return false;
        }

        // pobierz id nowego wiersza
        final Integer noweID = sterownik.getNewID("KLIENT");

        // zamien dane z java do sql
        Object[] daneSql = sterownik.javaToSQL(new Object[]{
            noweID,
            imie,
            nazwisko,
            email,
            login,
            haslo,
            0, // startowo nie aktywne
            (powiadomienia) ? 1 : 0
        });

        // dodaj do bazy 
        sterownik.insertIntoTable(Struktura.KLIENT, daneSql);

        // zamknij polaczenie
        sterownik.closeConnection();
        
        // jezeli wszystko ok wyslij wiadomosc z linkiem aktywujacym.
        wyslijWiadomoscAktywacyjna(email, login, noweID);
        
        return true;
    }

    private boolean sprawdzCzyLoginUnique(String login) throws SQLException {
        // polaczenie jest nawiazane

        final String query = "SELECT * FROM KLIENT WHERE LOGIN = " + sterownik.javaToSQL(login);
        Object[][] wynikZapytania = sterownik.queryDatabaseArray(query, Struktura.KLIENT_COLLUMNS);

        // zwroc wynik - unique jezeli zapytanie jest puste        
        return wynikZapytania.length == 0;
    }

    /**
     * Metoda wysylaja wiadomosc aktywujaca na podany adres email. Proste
     * generowanie za pomoca GET id klienta i loginu. Uzycie loginu zapewnia
     * pseudo bezpieczenstwo - klient nie moze w prosty sposob bawic sie
     * wpisujac nr konta. Aczkolwiek aktywacja kont jest raczej nie szkodliwa.
     *
     * @param email email klienta.
     * @param klientID id klieta.
     * @return true jesli operacja sie powiodla.
     */
    private boolean wyslijWiadomoscAktywacyjna(String email, String login, int klientID) throws MessagingException {
        
       Komunikator.wyslijMail(email, "Witaj w bibliotece", "Bardzo nam miło, że utworzyłeś konto w naszym serwisie. Od korzystania z serwisu dzieli Cię jeden drobny krok - aktywacja konta za pomocą tego linku: http://pawelglowacz.pl:8080/Biblioteka/AktywacjaServlet?id=" + String.valueOf(klientID) + "&l=" + login);

        return true;
    }

    /**
     * Wywolaj ta metode aby aktywowac konto klienta. Powinna zostac uzyta do
     * odebrania uzytkownika z linku aktywujacego.
     *
     * @param klientID
     * @param login
     * @return
     */
    public boolean aktywujKonto(int klientID, String login) throws ClassNotFoundException, SQLException {
        // polacz z baza danych
        sterownik.connectToDatabase();
        
        // sprawdz czy login istnieje.
        if (sprawdzCzyLoginUnique(login)) { // odwrotnie czyli oczekujemy false
            return false; // bledny login
        }
        
        // update pola w tabeli sql
        sterownik.queryUpdate("UPDATE KLIENT SET AKTYWNE = 1 WHERE ID_KLIENT=" + klientID + " AND LOGIN = " + sterownik.javaToSQL(login));
        
        // zamknij polaczenie
        sterownik.closeConnection();
        
        return true;
    }

}
