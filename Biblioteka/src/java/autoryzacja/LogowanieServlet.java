/*
 * Copyright 2018 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package autoryzacja;

import globalne.BezpiecznyServlet;
import globalne.Drukarka;
import globalne.Pola;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author marcin
 */
public class LogowanieServlet extends BezpiecznyServlet {

    
    @Override
    public boolean sprawdzDostep(HttpServletRequest request) {
        return request.getMethod().equals("POST");
    }

    @Override
    public Object[] pobierzDane(HttpServletRequest request) {
        return new String[]{
            request.getParameter("login"),
            request.getParameter("haslo"),
            request.getParameter("typ")
        };
    }

    @Override
    public void wykonajPrace(HttpServletRequest request, Object[] dane, HttpServletResponse response) throws ClassNotFoundException, SQLException, IOException {
        final Logowanie logowanie = new Logowanie(); // narzedzie
        int wynikLogowania = -1;
        Integer accountType = new Integer((String) dane[2]);
        switch (accountType) {
            case Pola.KLIENT:
                wynikLogowania = logowanie.logujKlienta((String) dane[0], (String) dane[1]);
                break;
            case Pola.PRACOWNIK:
                wynikLogowania = logowanie.logujPracownika((String) dane[0], (String) dane[1]);
                break;
            case Pola.ADMINISTRATOR:
                wynikLogowania = logowanie.logujAdministratora((String) dane[0], (String) dane[1]);
                break;
        }
      if (wynikLogowania == -1) { // logowanie nieudane, bledne dane
            przekierujPoPracy = false;
            response.getWriter().print(Drukarka.stworzStrone("Blad Logowania", "Podales bledne dane logowania lub konto jest nie aktywne, nie mozna sie zalogowac", "index.jsp"));
            return; // zakoncz wczesniej
        }      
        // zapisz do sesji dane jezeli logowanie sie powiodlo
        Pola.session.setAttribute(Pola.ACCOUNT_ID, wynikLogowania); // id konta zebysmy mogli stwierdzic czy i kto jest zalogowany
        Pola.session.setAttribute(Pola.ACCOUNT_TYPE, accountType); // typ konta zebysmy mogli stwierdzic kto jest zalogowany
        Pola.session.setAttribute(Pola.MENU, 0); // ustaw domyslny stan menu
    }

}
