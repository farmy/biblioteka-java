/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package autoryzacja;

import baza.Sterownik;
import baza.Struktura;
import java.sql.SQLException;

/**
 * Klasa odpowiadajaca za logowanie w serwisie.
 * @author marcin
 */
public class Logowanie {
    private final Sterownik sterownik = new Sterownik();
    
    
    
    private int loguj(String login, String haslo, String tabela, String[] kolumny, String dodatkowyWarunek) throws ClassNotFoundException, SQLException {
        // polaczenie z baza danych
        sterownik.connectToDatabase();
        
        // polaczenie ok, pobierz pasujace dane
        final String query = "SELECT * from " + tabela + " WHERE LOGIN = " + sterownik.javaToSQL(login) + " AND HASLO = " + sterownik.javaToSQL(haslo) + dodatkowyWarunek;
        final Object[][] queryResult = sterownik.queryDatabaseArray(query, kolumny);
        
        // zamknij polaczenie po zakonczeniu pracy.
        sterownik.closeConnection();
        
        // sprawdz wynik zapytania  - jezeli sie powiedzie powinnismy miec dokladnie 1 wiersz zapytania
        if (queryResult.length > 0) { // zapytanie zwrocilo conajmniej 1 wiersz
            return (int)queryResult[0][0]; // zwroc ID_KLIENT
        }                     
                
        return -1; // jezeli zapytanie nie zwrocilo 1 wiersza.
    }
    
    /**
     * Funkcja logujaca klienta.
     * @param login login klienta.
     * @param haslo haslo klienta.
     * @throws java.lang.ClassNotFoundException @see Sterownik
     * @throws java.sql.SQLException @see Sterownik
     * @return -1 jeśli błąd, id konta w innym przypadku.
     */
    public int logujKlienta(String login, String haslo) throws ClassNotFoundException, SQLException {
        return loguj(login, haslo, Struktura.KLIENT, Struktura.KLIENT_COLLUMNS, " AND AKTYWNE = 1");
    }
    
    /**
     * Funkcja logujaca klienta.
     * @param login login klienta.
     * @param haslo haslo klienta.
     * @throws java.lang.ClassNotFoundException @see Sterownik
     * @throws java.sql.SQLException @see Sterownik
     * @return -1 jeśli błąd, id konta w innym przypadku.
     */
    public int logujPracownika(String login, String haslo) throws ClassNotFoundException, SQLException {
        return loguj(login, haslo, Struktura.PRACOWNIK, Struktura.PRACOWNIK_COLLUMNS, "");
    }
    
    /**
     * Funkcja logujaca klienta.
     * @param login login klienta.
     * @param haslo haslo klienta.
     * @throws java.lang.ClassNotFoundException @see Sterownik
     * @throws java.sql.SQLException @see Sterownik
     * @return -1 jeśli błąd, id konta w innym przypadku.
     */
    public int logujAdministratora(String login, String haslo) throws ClassNotFoundException, SQLException {
        return loguj(login, haslo, Struktura.ADMINISTRATOR, Struktura.ADMINISTRATOR_COLLUMNS, "");
    }
    
    
    
}
