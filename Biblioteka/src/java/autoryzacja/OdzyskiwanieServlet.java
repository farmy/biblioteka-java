/*
 * Copyright 2018 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package autoryzacja;

import globalne.BezpiecznyServlet;
import globalne.Drukarka;
import globalne.Pola;
import java.io.IOException;
import java.sql.SQLException;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author marcin
 */
public class OdzyskiwanieServlet extends BezpiecznyServlet {

    @Override
    public boolean sprawdzDostep(HttpServletRequest request) {
        return request.getMethod().equals("POST"); // servlet formularzem.
    }

    @Override
    public Object[] pobierzDane(HttpServletRequest request) {
        return new Object[]{
            request.getParameter("email"),
            request.getParameter("typ"),};
    }

    @Override
    public void wykonajPrace(HttpServletRequest request, Object[] dane, HttpServletResponse response) throws ClassNotFoundException, SQLException, IOException {
        przekierujPoPracy = false;
        try {
            final Odzyskiwanie odzyskiwanie = new Odzyskiwanie();
            boolean sukces = false;
            switch (new Integer(dane[1].toString())) {
                case Pola.KLIENT:
                    sukces = odzyskiwanie.resetujHasloKlienta(dane[0].toString());
                    break;
                case Pola.PRACOWNIK:
                    sukces = odzyskiwanie.resetujHasloPracownika(dane[0].toString());
                    break;
                case Pola.ADMINISTRATOR:
                    sukces = odzyskiwanie.resetujHasloAdministratora(dane[0].toString());
                    break;
            }
            if (!sukces) {
                response.getWriter().print(Drukarka.stworzStrone("Blad", "Nie odnaleziono konta z podanym emailem!", "index.jsp"));
            } else {
                response.getWriter().print(Drukarka.stworzStrone("Sukces", "Haslo zostalo zresetowane i przeslane na podany email!", "index.jsp"));
            }
        } catch (MessagingException ex) {
            response.getWriter().print(Drukarka.stworzStrone("Blad wiadomosci", "Nie udalo sie przeslac wiadomosci email z nowym haslem!", "index.jsp"));
        }
    }

}
