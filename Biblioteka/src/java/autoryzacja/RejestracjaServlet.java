/*
 * Copyright 2018 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package autoryzacja;

import globalne.BezpiecznyServlet;
import globalne.Drukarka;
import java.io.IOException;
import java.sql.SQLException;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author marcin
 */
public class RejestracjaServlet extends BezpiecznyServlet {

    @Override
    public boolean sprawdzDostep(HttpServletRequest request) {
        return request.getMethod().equals("POST"); // servlet uruchomiony formularzem.
    }

    @Override
    public Object[] pobierzDane(HttpServletRequest request) {
        return new Object[]{
                request.getParameter("imie"),
                request.getParameter("nazwisko"),
                request.getParameter("email"),
                request.getParameter("login"),
                request.getParameter("haslo"),
                request.getParameter("newsletter") != null
            };
    }

    @Override
    public void wykonajPrace(HttpServletRequest request, Object[] dane, HttpServletResponse response) throws ClassNotFoundException, SQLException, IOException {
        przekierujPoPracy = false; // nie chcemy przekierowania
        try {
            new Rejestracja().dodajKontoKlienta(
                    dane[0].toString(),
                    dane[1].toString(),
                    dane[2].toString(),
                    dane[3].toString(),
                    dane[4].toString(),
                    (boolean)dane[5]); // konto zostalo dodane             
            response.getWriter().print(Drukarka.stworzStrone("Sukces", "Udalo sie zalozyc konto!", "index.jsp"));
        } catch (MessagingException ex) {
            response.getWriter().print(Drukarka.stworzStrone("Blad wiadomosci", "Nie udalo sie przeslac wiadomosci email z linkiem aktywujacym!" + ex.getMessage(), "index.jsp"));
        }
    }


}