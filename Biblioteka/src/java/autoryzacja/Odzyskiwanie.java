/*
 * Copyright 2017 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package autoryzacja;

import baza.Sterownik;
import baza.Struktura;
import java.sql.SQLException;
import java.util.Random;
import javax.mail.MessagingException;
import komunikacja.Komunikator;

/**
 * Klasa odpowiedzialna za funkcjonalność odzyskiwania kont.
 * @author marcin
 */
public class Odzyskiwanie {
    private final Sterownik sterownik = new Sterownik();
    
    private boolean resetujHaslo(String email, String tabela, String[] kolumny) throws ClassNotFoundException, SQLException, MessagingException {
        // polacz z baza danych
        sterownik.connectToDatabase();
        
        // sprawdz czy konto o podanym mailu istnieje
        if (sterownik.queryDatabaseArray("SELECT * FROM " + tabela + " WHERE EMAIL=" + sterownik.javaToSQL(email), kolumny).length == 0) {
            return false; 
        }
        
        // wygeneruj losowe haslo
        final String noweHaslo = generateRandomPassword();
        
        // aktualizuj wpis bazy danych
        sterownik.queryUpdate("UPDATE " + tabela + " SET HASLO = " + sterownik.javaToSQL(noweHaslo) + " WHERE EMAIL = " + sterownik.javaToSQL(email));
        
        // zakoncz polaczenie
        sterownik.closeConnection();
        
        // wyslij mail do uzytkownika z nowym haslem
        Komunikator.wyslijMail(email, "Odzyskiwanie konta - Biblioteka", "Witaj\nNa Twoja prośbę hasło zostało zresetowane, nowe hasło to: " + noweHaslo + "\nPozdrawiamy\nZespol Biblioteki");
        
        return true;
    }
    
    /**
     * Resetuje haslo do konta klienta, nowe haslo zostaje wyslane na podany email.
     * @param email email konta.
     * @return true jesli sie uda.
     * @throws ClassNotFoundException
     * @throws SQLException 
     * @throws javax.mail.MessagingException 
     */
    public boolean resetujHasloKlienta(String email) throws ClassNotFoundException, SQLException, MessagingException { // zakladajac, ze email jest unique
        return resetujHaslo(email, Struktura.KLIENT, Struktura.KLIENT_COLLUMNS);
    }
    
    /**
     * Resetuje haslo do konta Pracownika, nowe haslo zostaje wyslane na podany email.
     * @param email email konta.
     * @return true jesli sie uda.
     * @throws ClassNotFoundException
     * @throws SQLException 
     * @throws javax.mail.MessagingException 
     */
    public boolean resetujHasloPracownika(String email) throws ClassNotFoundException, SQLException, MessagingException {
        return resetujHaslo(email, Struktura.PRACOWNIK, Struktura.PRACOWNIK_COLLUMNS);        
    }

    /**
     * Resetuje haslo do konta Administratora, nowe haslo zostaje wyslane na podany email.
     * @param email email konta.
     * @return true jesli sie uda.
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws javax.mail.MessagingException
     */
    public boolean resetujHasloAdministratora(String email) throws ClassNotFoundException, SQLException, MessagingException {
        return resetujHaslo(email, Struktura.ADMINISTRATOR, Struktura.ADMINISTRATOR_COLLUMNS);        
    }
    
    // 33 to 126 normal chars
    private String generateRandomPassword() {
        char buffer[] = new char[16];
        Random r = new Random();
        for (int i = 0; i < 16; i++) {
            buffer[i] = (char) ((r.nextInt(126-33)+33));
            while (buffer[i] == 96) { // cant be "'" for its end of string in sql.
                buffer[i] = (char) ((r.nextInt(126-33)+33));
            }
        }
        return new String(buffer);
    }
    
    
    
    
    
    
}
