/*
 * Copyright 2017 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package komunikacja;

import baza.Sterownik;
import baza.Struktura;
import java.sql.SQLException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Odpowiedzialny za komunikacje zewnetrzna z uzytkownikiem.
 *
 * @author marcin
 */
public class Komunikator {

    /**
     * Wyslij email.
     *
     * @param email adres na ktory ma zostac wyslana wiadomosc.
     * @param temat temat wiadomosci.
     * @param tresc tresc wiadomosci.
     * @return true jesli operacja sie powiodla.
     * @throws MessagingException wyjatek, jezeli nie udalo sie wyslac maila.
     */
    public static boolean wyslijMail(String email, String temat, String tresc) throws MessagingException {
        final String username = "smotorzysci@gmail.com";
        final String password = "krolobwodnicy";

        Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress("from-email@gmail.com"));
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(email));
        message.setSubject(temat);
        message.setText(tresc);

        Transport.send(message);


        return true;
    }

    /**
     * Wyslij powiadomienia o dodaniu nowej ksiazki do wszystkich klientow z
     * aktywnymi powiadomieniami.
     *
     * @param sterownik referencja na sterownik bazy danych.
     * @param autor autor ksiazki.
     * @param tytul tytul ksiazki.
     * @return true jesli operacja sie powiodla.
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     * @throws javax.mail.MessagingException
     */
    public boolean wyslijPowiadomienia(Sterownik sterownik, String autor, String tytul) throws ClassNotFoundException, SQLException, MessagingException {
        // polacz z baza danych
        sterownik.connectToDatabase();

        // pobierz liste klientow z aktywnymi powiadomieniami
        Object[][] tablicaKlientow = sterownik.queryDatabaseArray("SELECT * FROM " + Struktura.KLIENT + " WHERE POWIADOMIENIE_AKTYWNE=true", Struktura.KLIENT_COLLUMNS);

        // zakoncz polaczenie
        sterownik.closeConnection();

        // wyslij maila do kazdego klienta
        for (Object[] wiersz : tablicaKlientow) {
            Komunikator.wyslijMail(wiersz[3].toString(), "Nowa ksiazka w serwisie", "Witaj " + wiersz[1] + "\nWlasnie dodano nowa ksiazke do serwisu:\n" + autor + " " + tytul + "\nZapraszamy na lekturę\nZespół biblioteki");
        }

        return true;
    }

}
