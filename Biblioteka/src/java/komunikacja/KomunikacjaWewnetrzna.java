/*
 * Copyright 2017 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package komunikacja;

import baza.Sterownik;
import baza.Struktura;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Klasa odpowiedzialna za funkcjonalność komunikacji wewnętrznej serwisu -
 * pytania i odpowiedzi, klienta i pracownika.
 *
 * @author marcin
 */
public class KomunikacjaWewnetrzna {

    // Wersja uproszczona, pytania i odpowiedzi sa w 1 tabeli, bez ograniczen.
    private final Sterownik sterownik = new Sterownik();
    /**
     * Typ wiadomosci.
     */
    private static final String PYTANIE = "Pytanie", ODPOWIEDZ = "Odpowiedz";

    private boolean wyslijWiadomosc(int klientID, int pracownikID, String tresc, String typ) throws ClassNotFoundException, SQLException {
        // polacz z baza danych
        sterownik.connectToDatabase();

        // pobierz id pytania
        final int noweID = sterownik.getNewID(Struktura.WIADOMOSC);

        // get current date
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String date = sdf.format(Calendar.getInstance().getTime());
        Object sqlDate = "STR_TO_DATE('" + date + "', '%Y/%m/%d %T')";

        // stworz nowy wiersz i zamien go do formy sql
        final Object wiersz[] = sterownik.javaToSQL(new Object[]{
            noweID,
            klientID,
            pracownikID,
            tresc,
            typ,
            "data"
        });
        wiersz[5] = sqlDate; // funkcja nie moze byc traktowana jako string

        // dodaj wpis do bazy danych
        sterownik.insertIntoTable(Struktura.WIADOMOSC, wiersz);

        // zakoncz polaczenie
        sterownik.closeConnection();

        return true;
    }

    /**
     * Dodaje pytanie do bazy danych, powinno byc wywolane przez klienta.
     *
     * @param klientID id konta klienta.
     * @param pracownikID id konta pracownika, powinno byc wybrane poprzez drop
     * menu.
     * @param tresc tresc pytania.
     * @return true jezeli udalo sie dodac do bazy danych.
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public boolean wyslijPytanie(int klientID, int pracownikID, String tresc) throws ClassNotFoundException, SQLException {
        return wyslijWiadomosc(klientID, pracownikID, tresc, PYTANIE);
    }

    /**
     * Dodaje odpowiedz do bazy danych, powinno byc wywolane przez pracownika.
     *
     * @param klientID id konta klienta, powinno byc odczytane z listy pytan.
     * @param pracownikID id konta pracownika menu.
     * @param tresc tresc pytania.
     * @return true jezeli udalo sie dodac do bazy danych.
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public boolean wyslijOdpowiedz(int klientID, int pracownikID, String tresc) throws ClassNotFoundException, SQLException {
        return wyslijWiadomosc(klientID, pracownikID, tresc, ODPOWIEDZ);
    }

    /**
     * Pobierz liste pytan dla danego pracownika.
     *
     * @param pracownikID id pracownika
     * @return lista pytan.
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public Object[][] pobierzListePytanPracownika(int pracownikID) throws ClassNotFoundException, SQLException {
        // polacz z baza danych
        sterownik.connectToDatabase();

        // pobierz z bazy wszystkie pytania dla danego pracownika.
        final Object[][] wynik = sterownik.queryDatabaseArray("select ID_WIADOMOSC,TRESC, TYP, DATA_UTWORZENIA as data, KLIENT.IMIE as imie_klienta, KLIENT.NAZWISKO as nazwisko_klienta, KLIENT.EMAIL as email_klienta, KLIENT.ID_KLIENT from KLIENT, WIADOMOSC\n"
                + "where WIADOMOSC.ID_KLIENT = KLIENT.ID_KLIENT AND ID_PRACOWNIK = " + pracownikID, new String[]{
                    "ID_WIADOMOSC",
                    "tresc",
                    "typ",
                    "data",
                    "imie_klienta",
                    "nazwisko_klienta",
                    "email_klienta",
                    "ID_KLIENT",
                });

        // zakoncz polaczenie
        sterownik.closeConnection();

        return wynik;
    }

    /**
     * Pobierz liste pytan dotyczaca danego klienta.
     *
     * @param klientID
     * @return
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    public Object[][] pobierzListePytanKlienta(int klientID) throws ClassNotFoundException, SQLException {
        // polacz z baza danych
        sterownik.connectToDatabase();

        // pobierz z bazy wszystkie pytania dla danego pracownika.
        final Object[][] wynik = sterownik.queryDatabaseArray("select ID_WIADOMOSC,TRESC, TYP, DATA_UTWORZENIA as data, PRACOWNIK.IMIE as imie_pracownika, PRACOWNIK.NAZWISKO as nazwisko_pracownika, PRACOWNIK.EMAIL as email_pracownika from PRACOWNIK, WIADOMOSC\n"
                + "where WIADOMOSC.ID_PRACOWNIK = PRACOWNIK.ID_PRACOWNIK AND ID_KLIENT = " + klientID, new String[]{
                    "ID_WIADOMOSC",
                    "tresc",
                    "typ",
                    "data",
                    "imie_pracownika",
                    "nazwisko_pracownika",
                    "email_pracownika",});

        // zakoncz polaczenie
        sterownik.closeConnection();

        return wynik;
    }

}
