/*
 * Copyright 2017 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package administracja;

import baza.Sterownik;
import baza.Struktura;
import java.sql.SQLException;
import konta.Profile;

/**
 * Klasa odpowiedzialna za operacje administracyjne na kontach uzytkownikow.
 *
 * @author marcin
 */
public class ZarzadzanieKontami extends Profile {

    private final Sterownik sterownik = new Sterownik();

    private Object[][] pobierzKonta(String nazwa, String[] kolumny) throws ClassNotFoundException, SQLException {
        // polacz z baza danych
        sterownik.connectToDatabase();

        Object[][] wynik = sterownik.queryDatabaseArray("SELECT * FROM " + nazwa, kolumny);

        // zakoncz polaczenie
        sterownik.closeConnection();

        return wynik;
    }

    /**
     * Pobierz wszystkie konta typu klient.
     *
     * @return wszystkie konta typu klient.
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public Object[][] pobierzKontaKlientow() throws ClassNotFoundException, SQLException {
        return pobierzKonta(Struktura.KLIENT, Struktura.KLIENT_COLLUMNS);
    }

    /**
     * Pobierz wszystkie konta typu pracownik.
     *
     * @return wszystkie konta typu pracownik.
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public Object[][] pobierzKontaPracownikow() throws ClassNotFoundException, SQLException {
        return pobierzKonta(Struktura.PRACOWNIK, Struktura.PRACOWNIK_COLLUMNS);
    }

    /**
     * Pobierz wszystkie konta typu administrator.
     *
     * @return wszystkie konta typu administrator.
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public Object[][] pobierzKontaAdministratorow() throws ClassNotFoundException, SQLException {
        return pobierzKonta(Struktura.ADMINISTRATOR, Struktura.ADMINISTRATOR_COLLUMNS);
    }

    private boolean dodajKonto(String nazwa, Object dane[]) throws ClassNotFoundException, SQLException {
        // polacz z baza danych
        sterownik.connectToDatabase();

        // pobierz id nowego wiersza;
        final int noweID = sterownik.getNewID(nazwa);

        // przetworz wartosci java do sql
        Object daneTemp[] = new Object[dane.length + 1]; // potrzebujemy do danych dodac id
        daneTemp[0] = noweID;
        for (int i = 1; i < daneTemp.length; i++) { // skopiuj zawartosc danych
            daneTemp[i] = dane[i - 1];
        }
        daneTemp = sterownik.javaToSQL(daneTemp);

        // aktualizuj baze danych
        sterownik.insertIntoTable(nazwa, daneTemp);

        // zakoncz polaczenie
        sterownik.closeConnection();

        return true;
    }

    // todo: można dodać restrykcje - w parametrze przyjmowac konkretne dane, a dopiero w dodajKonto przekazywac nowa tablice.
    /**
     * Dodaj konto klienta do bazy danych.
     *
     * @param dane dane konta.
     * @return true jesli operacja sie powiodla.
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public boolean dodajKontoKlienta(Object[] dane) throws ClassNotFoundException, SQLException {
        return dodajKonto(Struktura.KLIENT, dane);
    }

    /**
     * Dodaj konto Pracownika do bazy danych.
     *
     * @param dane dane konta.
     * @return true jesli operacja sie powiodla.
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public boolean dodajKontoPracownika(Object[] dane) throws ClassNotFoundException, SQLException {
        return dodajKonto(Struktura.PRACOWNIK, dane);
    }

    /**
     * Dodaj konto Administratora do bazy danych.
     *
     * @param dane dane konta.
     * @return true jesli operacja sie powiodla.
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public boolean dodajKontoAdministratora(Object[] dane) throws ClassNotFoundException, SQLException {
        return dodajKonto(Struktura.ADMINISTRATOR, dane);
    }

    private static final int KLIENT = 0, PRACOWNIK = 1, ADMIN = 2;

    private boolean usunKonto(String nazwa, Object kontoID, int foreigns) throws ClassNotFoundException, SQLException {
        // polacz z baza danych
        sterownik.connectToDatabase();

        // usun klucze obce
        switch (foreigns) {
            case KLIENT:
                // usun rekordy powiazane z tabeli chce przeczytac
                sterownik.queryUpdate("DELETE FROM KSIAZKA_CHCE_PRZECZYTAC WHERE ID_KLIENT=" + kontoID);

                // usun rekordy powiazane z tabeli przeczytane
                sterownik.queryUpdate("DELETE FROM KSIAZKA_PRZECZYTANA WHERE ID_KLIENT=" + kontoID);
            case PRACOWNIK: // wykonaj dodatkowo do klienta lub dla pracownika - wiadomosci nalezy usunuac w obydwoch przypadkach.
                // usun rekordy powiazane z tabeli WIADOMOSC
                sterownik.queryUpdate("DELETE FROM WIADOMOSC WHERE ID_" + nazwa + "=" + kontoID);
                break;
            case ADMIN: // admin nie posiada kluczow obcychh
                break;
        }

        // aktualizuj baze danych
        sterownik.queryUpdate("DELETE FROM " + nazwa + " WHERE ID_" + nazwa + "=" + kontoID);

        // zakoncz polaczenie
        sterownik.closeConnection();

        return true;
    }

    /**
     * Usun konto klienta z bazy danych.
     *
     * @param kontoID id konta.
     * @return true jesli operacja sie powiodla.
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public boolean usunKontoKlienta(int kontoID) throws ClassNotFoundException, SQLException {
        return usunKonto(Struktura.KLIENT, kontoID, KLIENT);
    }

    /**
     * Usun konto Pracownika z bazy danych.
     *
     * @param kontoID id konta.
     * @return true jesli operacja sie powiodla.
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public boolean usunKontoPracownika(int kontoID) throws ClassNotFoundException, SQLException {
        return usunKonto(Struktura.PRACOWNIK, kontoID, PRACOWNIK);
    }

    /**
     * Usun konto Administratora z bazy danych.
     *
     * @param kontoID id konta.
     * @return true jesli operacja sie powiodla.
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public boolean usunKontoAdministratora(int kontoID) throws ClassNotFoundException, SQLException {
        return usunKonto(Struktura.ADMINISTRATOR, kontoID, ADMIN);
    }

}
