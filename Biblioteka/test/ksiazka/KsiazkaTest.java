/*
 * Copyright 2017 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ksiazka;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author marcin
 */
public class KsiazkaTest {
    
    public KsiazkaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // tylko annotowana funkcja jest uruchamiana.
    
    public void testZarzadzaniaKsiazkami() throws Exception {
        final ZarzadzanieKsiazkami zarzadzanieKsiazkami = new ZarzadzanieKsiazkami();
        zarzadzanieKsiazkami.dodajKsiazke(new Object[]{
           "Dziady", 
           "Adam Mickiewicz", 
           "sciezka" 
        }, false);
        zarzadzanieKsiazkami.edytujKsiazke(new Object[]{
           "Dziady", 
           "Adam Mickiewicz", 
           "sciezka321" 
        }, 1);
        //zarzadzanieKsiazkami.usunKsiazke(1);
        System.out.println("ksiazki:\n");
        for (Object[] wiersz : zarzadzanieKsiazkami.pobierzKsiazki()) {
            for (Object przedmiot : wiersz) {
                System.out.print(przedmiot + ", ");
            }
            System.out.println();
        }
    }
    
    public void testZarzadzaniaKsiazkamiDoPrzeczytania() throws Exception {
        final ZarzadzanieKsiazkamiDoPrzeczytania zarzadzanieKsiazkamiDoPrzeczytania = new ZarzadzanieKsiazkamiDoPrzeczytania();
        //zarzadzanieKsiazkamiDoPrzeczytania.dodajKsiazke(1, 1);
        zarzadzanieKsiazkamiDoPrzeczytania.usunKsiazke(1);
        System.out.println("ksiazki:\n");
        for (Object[] wiersz : zarzadzanieKsiazkamiDoPrzeczytania.pobierzKsiazki(1)) {
            for (Object przedmiot : wiersz) {
                System.out.print(przedmiot + ", ");
            }
            System.out.println();
        }
    }
    
    public void testWypozyczania() throws Exception {
        final Wypozyczanie wypozyczanie = new Wypozyczanie();
        //wypozyczanie.wypozyczKsiazke("Adam Mickiewicz", "Dziady", 1);
        System.out.println(wypozyczanie.sprawdzDostepDoKsiazki(1, "Adam Mickiewicz", "Dziady"));
    }
    
    public void testZarzadzaniaKsiazkamiPrzeczytanymi() throws Exception {
        final ZarzadzanieKsiazkamiPrzeczytanymi zarzadzanieKsiazkamiPrzeczytanymi = new ZarzadzanieKsiazkamiPrzeczytanymi();
        System.out.println("ksiazki:\n");
        for (Object[] wiersz : zarzadzanieKsiazkamiPrzeczytanymi.pobierzKsiazki()) {
            for (Object przedmiot : wiersz) {
                System.out.print(przedmiot + ", ");
            }
            System.out.println();
        }
    }
    
    @Test
    public void testCzytnika() throws Exception {
        final Czytnik czytnik = new Czytnik();
        System.out.println(czytnik.odczytajKsiazke("/home/marcin/NetBeansProjects/WebBookDownloader/README.TXT"));
    }
    
    
    
    
    
    
}
