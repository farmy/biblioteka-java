/*
 * Copyright 2017 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package konta;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author marcin
 */
public class KontaTest {

    public KontaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testKont() throws Exception {
        final Profile profile = new Profile();

        profile.aktualizujDaneKlienta(new Object[]{"MAREK", "NOWAK", "M@NOWAK.COM", "KLIENT", "KLIENT123", true, true
                }, 1);
        
        profile.aktualizujDanePracownika(new Object[]{"Pracus", "Tests", "prac@test.COM", "PRACOWNIK2", "PRACOWNIK123"
                }, 1);
        
        profile.aktualizujDaneAdministratora(new Object[]{"Admin", "Harts", "admin@test.COM", "ADMINISTRATOR3", "ADMINISTRATOR123"
                }, 1);

        System.out.println("admin");
        for (Object item : profile.pobierzDaneAdministratora(1)) {
            System.out.print(item + ", ");
        }

        System.out.println("\npracownik");
        for (Object item : profile.pobierzDanePracownika(1)) {
            System.out.print(item + ", ");
        }

        System.out.println("\nklient");
        for (Object item : profile.pobierzDaneKlienta(1)) {
            System.out.print(item + ", ");
        }

    }
}
