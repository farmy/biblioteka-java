/*
 * Copyright 2017 marcin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package administracja;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author marcin
 */
public class AdministracjaTest {
    
    public AdministracjaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

   
    @Test
    public void testZarzadzaniaKontami() throws Exception {
        final ZarzadzanieKontami zarzadzanieKontami = new ZarzadzanieKontami();
        /*zarzadzanieKontami.dodajKontoKlienta(new Object[]{
            "Imie",
            "Nazwisko",
            "Email",
            "login",
            "haslo",
            true,
            true,
        });
        
        zarzadzanieKontami.dodajKontoPracownika(new Object[]{
            "Imie",
            "Nazwisko",
            "Email",
            "login",
            "haslo"
        });
        
        zarzadzanieKontami.dodajKontoAdministratora(new Object[]{
            "Imie",
            "Nazwisko",
            "Email",
            "login",
            "haslo"
        });*/
        
        zarzadzanieKontami.usunKontoKlienta(3);
        zarzadzanieKontami.usunKontoPracownika(2);
        zarzadzanieKontami.usunKontoAdministratora(2);
        
        System.out.println("klienci:\n");
        for (Object[] wiersz : zarzadzanieKontami.pobierzKontaKlientow()) {
            for (Object przedmiot : wiersz) {
                System.out.print(przedmiot + ", ");
            }
            System.out.println();
        }
        System.out.println("pracownicy:\n");
        for (Object[] wiersz : zarzadzanieKontami.pobierzKontaPracownikow()) {
            for (Object przedmiot : wiersz) {
                System.out.print(przedmiot + ", ");
            }
            System.out.println();
        }
        System.out.println("administratorzy:\n");
        for (Object[] wiersz : zarzadzanieKontami.pobierzKontaAdministratorow()) {
            for (Object przedmiot : wiersz) {
                System.out.print(przedmiot + ", ");
            }
            System.out.println();
        }
    }
}
